﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Designed for Spite Particals so the partical always faces the camera
/// </summary>
public class ParticleLookAt : MonoBehaviour
{
    private void LateUpdate()
    {
        gameObject.transform.LookAt(Camera.main.gameObject.transform.position);
    }
}

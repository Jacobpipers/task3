﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleExampleController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Hit Space to spawn partical explosion");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<Particle>().SpawnParticle(gameObject.transform.position);
        }
    }
}

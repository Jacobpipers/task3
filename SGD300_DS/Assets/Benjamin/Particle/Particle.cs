﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Particle : MonoBehaviour
{
    /// <summary>
    /// Preset particle patterns
    /// </summary>
    private enum ParticlePreset 
    { 
        custom,
        explosion,
        fire,
        drip,
    }

    // particle TO SPAWN
    [SerializeField] private GameObject particleObject;
    [Space(5)]

    // BASIC SETTINGS
    [Header("Basic Settings")]
    [SerializeField] private bool runOnStart = false;
    [SerializeField] private bool debugMode = false;

    [SerializeField] private Vector3 force = new Vector3(100, 100, 100);
    [SerializeField] private Vector3 particleScale = new Vector3(0.2f, 0.2f, 0.2f);
    [SerializeField] private float lifespan = 5;

    [SerializeField] private bool looping = false;
    [SerializeField] private bool gravity = false;
    [SerializeField] private bool collisions = false;
    [SerializeField] private bool randomRotation = true;
    [Space(5)]

    // PRESET SETTINGS
    [Header("Preset Settings")]
    [SerializeField] ParticlePreset particlePreset = ParticlePreset.explosion;
    [Space(5)]

    // CUSTOMIZE SETTINGS
    [Header("Custom Preset Settings")]
    [SerializeField] private Vector3 areaSpawning = Vector3.zero;
    [SerializeField] private bool randomForce = false;
    [SerializeField] private float customGravity = 0;
    [SerializeField] private float spawnRate = 1;
    [SerializeField] private int burstNumber = 3;

    private void Start()
    {
        // Spawns particle when the scene starts
        if (runOnStart)
        {
            SpawnParticle(gameObject.transform.position);
        }   
    }

    /// <summary>
    /// Spawn particles with settings of particle preset or custom
    /// </summary>
    /// <param name="position"></param>
    /// <param name="particlePreset"></param>
    public void SpawnParticle(Vector3 position)
    {
        // Setup particle object with basic settings
        GameObject newParticleObject = particleObject;
        newParticleObject.GetComponent<Rigidbody>().useGravity = gravity;
        newParticleObject.GetComponent<Collider>().enabled = collisions;
        newParticleObject.transform.localScale = particleScale;

        switch (particlePreset)
        {
            //----------------- CUSTOM -----------------
            case ParticlePreset.custom:
                if (debugMode) Debug.Log(ParticlePreset.custom);
                StartCoroutine(PresetCustom(position, particleObject));
                break;

            //----------------- EXPLOSION -----------------
            case ParticlePreset.explosion:
                if (debugMode) Debug.Log(ParticlePreset.explosion); // Alert the particle has been spawned
                StartCoroutine(PresetExplosion(position, particleObject));
                break;

            //----------------- FIRE -----------------
            case ParticlePreset.fire:
                if (debugMode) Debug.Log(ParticlePreset.fire);
                StartCoroutine(PresetFire(position, particleObject));

                break;
                
            //----------------- DRIP -----------------
            case ParticlePreset.drip:
                if (debugMode) Debug.Log(ParticlePreset.drip);
                StartCoroutine(PresetDrip(position, particleObject));
                break;

            //----------------- NO PRESET -----------------
            default:
                Debug.LogError("Missing particle Preset");
                break;
        }
    }

    /// <summary>
    /// Create new particle or get particle from object pool
    /// </summary>
    /// <param name="particlePool">The object pool to get the particle from</param>
    /// <param name="particleObject">Type of partcal to create</param>
    private GameObject GetParticle(GameObject particleObject, Queue<GameObject> particlePool = null)
    {
        GameObject particle;
        if (particlePool.Count < 1 || particlePool == null)
        {
            // Creates new object because particle pool is empty
            particle = Instantiate(particleObject);
            particle.transform.SetParent(gameObject.transform);
            particle.SetActive(false);
        }
        else
        {
            // Depools from particle object pool
            particle = particlePool.Dequeue();
        }
        // Returns the particle that was found/created
        return particle;
    }

    /// <summary>
    /// Coroutine ->
    /// Destroy particle by either destroying the object or adding it to a object pool
    /// </summary>
    /// <param name="particle">The particle to destroy</param>
    /// <param name="delay">The delay to destroy the particle</param>
    /// <param name="particlePool">The object pool to add the particle to</param>
    private IEnumerator DestroyParticle(GameObject particle, float delay, Queue<GameObject> particlePool = null)
    {
        // Delay in destroying the particle
        yield return new WaitForSeconds(delay);

        // Only object pool if the particle is looping
        if (looping && particlePool != null)
        {
            // Add particle to object pool
            particle.GetComponent<Rigidbody>().velocity = Vector3.zero;
            particle.SetActive(false);
            particlePool.Enqueue(particle);
        } else
        {
            // Destroy particle
            Destroy(particle);
        }
    }

    /// <summary>
    /// Coroutine ->
    /// Destroy a list of particles by either destroying the object or adding it to a object pool
    /// </summary>
    /// <param name="particle">The particle to destroy</param>
    /// <param name="delay">The delay to destroy the particle</param>
    /// <param name="particlePool">The object pool to add the particle to</param>
    private IEnumerator DestroyParticleGroup(List<GameObject> particles, float delay, Queue<GameObject> particlePool = null)
    {
        // Delay in destroying the particle
        yield return new WaitForSeconds(delay);

        // Loop through each particle in list to destroy
        foreach (GameObject p in particles)
        {
            // Only object pool if the particle is looping
            if (looping && particlePool != null)
            {
                // Add particle to object pool
                p.GetComponent<Rigidbody>().velocity = Vector3.zero;
                p.SetActive(false);
                particlePool.Enqueue(p);
            }
            else
            {
                // Destroy particle
                Destroy(p);
            }
        }
    }


    // -----------------------------------------------------------------
    //                           PARTICLE PRESETS
    // -----------------------------------------------------------------

    /// <summary>
    /// Coroutine ->
    /// Particle Preset of Custom
    /// Custom particle which follows the settings given
    /// </summary>
    /// <param name="position">Position of particles</param>
    /// <param name="particleObject">The particle to spawn</param>
    private IEnumerator PresetCustom(Vector3 position, GameObject particleObject)
    {
        // particle Object Pool
        Queue<GameObject> particlePool = new Queue<GameObject>();

        // While loop - Will only run once if looping is not true
        int p = 0;
        while (p < 1 || looping == true)
        {
            // particle list, this is a list of objects that will be destroyed when Destroy particles is called
            List<GameObject> particlList = new List<GameObject>();

            // ---------------------- SPAWNING POSITION SETUP ----------------------
            Vector3 spawnPosition;
            if (areaSpawning == Vector3.zero)
            {
                // Spawn at direct position
                spawnPosition = position;
            }
            else
            {
                // Spawn at random location within bounds of spawn area
                Vector3 range = areaSpawning / 2.0f;
                Vector3 randomRange = new Vector3(Random.Range(-range.x, range.x), Random.Range(-range.y, range.y), Random.Range(-range.z, range.z));
                spawnPosition = position + randomRange;
            }
            // ----------------------------------------------------------------------

            // Spawns "burstNumber" of particles at once at spawn position
            for (int i = 0; i < burstNumber; i++)
            {
                GameObject particle = GetParticle(particleObject, particlePool);
                particle.transform.position = spawnPosition;
                // Sets random rotation if its turned on
                if (randomRotation) { particle.transform.rotation = new Quaternion(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180), 180); }
                particle.SetActive(true);
                // Rigidbody reference for new particle
                Rigidbody particleRB = particle.GetComponent<Rigidbody>();

                // Change force if random force is on
                Vector3 newForce;
                if (randomForce)
                {
                    newForce = new Vector3(Random.Range(-force.x, force.x), Random.Range(-force.y, force.y), Random.Range(-force.z, force.z));
                } else
                {
                    newForce = force;
                }
                particleRB.AddForce(newForce, ForceMode.Acceleration);

                // Set custom gravity
                if (customGravity != 0)
                {
                    particleRB.useGravity = false;
                    particleRB.AddForce(new Vector3(0, customGravity, 0), ForceMode.Acceleration);
                }

                // Add particle to destroy list - "particleList"
                particlList.Add(particle);
            }
            // Destroy particles after lifespan has ended and adds the particle to the object pool
            StartCoroutine(DestroyParticleGroup(particlList, lifespan, particlePool));
            // Add one for each time the particle loops
            p++;
            // Minimum spawn rate is 0.1f
            if (spawnRate < 0.1f) { spawnRate = 0.1f; }
            yield return new WaitForSeconds(spawnRate);
        }

        if (debugMode)
        {
            Debug.Log(ParticlePreset.custom + " looped " + p + " times.");
        }
    }

    /// <summary>
    /// Coroutine ->
    /// Particle Preset of Explosion
    /// Several particles that spawn then burst out in random directions
    /// </summary>
    /// <param name="position">Position of particles</param>
    /// <param name="particleObject">The particle to spawn</param>
    private IEnumerator PresetExplosion(Vector3 position, GameObject particleObject)
    {
        Queue<GameObject> particlePool = new Queue<GameObject>();
        int p = 0;
        while (p < 1 || looping == true)
        {
            List<GameObject> particlList = new List<GameObject>();
            for (int i = 0; i < 10; i++)
            {
                GameObject particle = GetParticle(particleObject, particlePool);
                particle.transform.position = position;
                if (randomRotation) { particle.transform.rotation = new Quaternion(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180), 180); }
                particle.SetActive(true);
                particle.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-force.x, force.x), Random.Range(-force.y, force.y), Random.Range(-force.z, force.z)), ForceMode.Acceleration);
                particlList.Add(particle);
            }
            StartCoroutine(DestroyParticleGroup(particlList, lifespan, particlePool));
            p++;
            yield return new WaitForSeconds(0.5f);
        }

        if (debugMode)
        {
            Debug.Log(ParticlePreset.explosion + " looped " + p + " times.");
        }
    }

    /// <summary>
    /// Coroutine ->
    /// Particle Preset of Fire
    /// Spawning particles that rise like fire or smoke
    /// </summary>
    /// <param name="position">Position of particles</param>
    /// <param name="particleObject">The particle to spawn</param>
    private IEnumerator PresetFire(Vector3 position, GameObject particleObject)
    {
        Queue<GameObject> particlePool = new Queue<GameObject>();
        int i = 0;
        while (i < 1 || looping == true)
        {
            GameObject particle = GetParticle(particleObject, particlePool);
            particle.GetComponent<Rigidbody>().useGravity = false;
            particle.transform.position = position;
            if (randomRotation) { particle.transform.rotation = new Quaternion(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180), 180); }
            particle.SetActive(true);
            particle.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-15, 15), Random.Range(0, force.y), Random.Range(-15, 15)), ForceMode.Acceleration);
            StartCoroutine(DestroyParticle(particle, Random.Range(2, 5) , particlePool));
            i++;
            yield return new WaitForSeconds(Random.Range(0.1f, 1));
        }

        if (debugMode)
        {
            Debug.Log(ParticlePreset.fire + " looped " + i + " times.");
        }
    }

    /// <summary>
    /// Coroutine ->
    /// Particle Preset of Drip
    /// Spawning single particles over time with gravity
    /// </summary>
    /// <param name="position">Position of particles</param>
    /// <param name="particleObject">The particle to spawn</param>
    private IEnumerator PresetDrip(Vector3 position, GameObject particleObject)
    {
        Queue<GameObject> particlePool = new Queue<GameObject>();
        int i = 0;
        while (i < 1 || looping == true)
        {
            GameObject particle = GetParticle(particleObject, particlePool);
            particle.GetComponent<Rigidbody>().useGravity = true;
            particle.transform.position = position;
            if (randomRotation) { particle.transform.rotation = new Quaternion(Random.Range(-180, 180), Random.Range(-180, 180), Random.Range(-180, 180), 180); }
            particle.SetActive(true);
            particle.GetComponent<Rigidbody>().AddForce(new Vector3(Random.Range(-5, 5), 0, Random.Range(-5, 5)), ForceMode.Acceleration);
            StartCoroutine(DestroyParticle(particle, lifespan, particlePool));
            i++;
            yield return new WaitForSeconds(Random.Range(0.1f, 2.5f));
        }

        if (debugMode)
        {
            Debug.Log(ParticlePreset.fire + " looped " + i + " times.");
        }
    }

    // -----------------------------------------------------------------
    //                      END OF PARTICLE PRESETS
    // -----------------------------------------------------------------

#if UNITY_EDITOR
    // Debugs for unity editor
    void OnDrawGizmos()
    {
        if (debugMode == true)
        {
            Gizmos.color = Color.magenta;
            if (areaSpawning != Vector3.zero)
            {
                Gizmos.DrawWireCube(transform.position, areaSpawning);
            }
            else
            {
                Gizmos.DrawCube(transform.position, new Vector3(0.1f, 0.1f, 0.1f));
            }
        }
    }
#endif

}

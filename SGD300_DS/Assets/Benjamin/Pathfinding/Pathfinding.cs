﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic Pathfinding
/// - StopMoving() Forces object to stop following path
/// - Warp(Vector3 targetPosition) Teleports object to targetPosition
/// - Move(Vector3 targetPosition) Moves object along path to targetPosition
/// </summary>
public class Pathfinding : MonoBehaviour
{
    /// <summary>
    /// This enum determines which state of pathfinding the actor is currently in.
    /// </summary>
    public enum PathfindingEnum
    {
        moving,
        stopped
    }

    [Header("Nav Settings")]
    [Tooltip("Max range that to search, this can be limited by how often Move() is called.")]
    [SerializeField] private int searchRange = 60; // Max range that to search, this can be limited by how often Move() is called.
    [Tooltip("Layer that raycasts hit to check ground")]
    [SerializeField] private LayerMask movementLayer; // Layer that raycasts hit to check ground
    [Tooltip("Speed the object will move along the path")]
    [SerializeField] public float moveSpeed = 0.1f; // Speed the object will move along the path
    [Tooltip("Whether or not the rotation will be controlled to look along path")]
    [SerializeField] private bool lookAtPath = true; // Whether or not the rotation will be controlled to look along path
    [Tooltip("Rotation smoothing when rotation is following path")]
    [SerializeField] private float rotationSmoothing = 5f; // Rotation smoothing when rotation is following path
    [Tooltip("The height of the object, this will determine what it can walk under")]
    [SerializeField] private float objectHeight = 1.8f; // The height of the object, this will determine what it can walk under
    [Tooltip("The width of the object, this will determine how close to the wall the object can get")]
    [SerializeField] private float objectRadius = 0.5f; // The width of the object, this will determine how close to the wall the object can get
    [Tooltip("Debug mode - Displays Gizmos")]
    [SerializeField] private bool debugMode = false; // Debug mode - Displays Gizmos


    // Private variables
    private Dictionary<Vector3, PathfindingNode> navMain = new Dictionary<Vector3, PathfindingNode>(); // Path to follow, this path is replaced with navSecondary once navSecondary path is found
    private Dictionary<Vector3, PathfindingNode> navSecondary = new Dictionary<Vector3, PathfindingNode>();
    private Queue<PathfindingNode> generateQueue = new Queue<PathfindingNode>(); // Generate node queue
    private Vector3 currentPosNode; // Current position on path
    private bool pathFound = false; // True when the path is found
    private bool canMove = true; // Can the actor move
    private bool canCallMove = true; // Can the move function be called
    private PathfindingEnum pfindingState = PathfindingEnum.stopped;

    // Read this to check whether or not the actor is moving
    public PathfindingEnum pathfindingState
    {
        get { return pfindingState; }
    }

    /// <summary>
    /// Forces object to stop pathfinding and stand still.
    /// </summary>
    public void StopMoving()
    {
        pfindingState = PathfindingEnum.stopped;
        canMove = false;
    }

    /// <summary>
    /// Teleport the object directly to position.
    /// </summary>
    /// <param name="newPosition"> Position to teleport to. </param>
    public void Warp(Vector3 newPosition)
    {
        if (Physics.Raycast(new Vector3(Mathf.Round(newPosition.x), newPosition.y, Mathf.Round(newPosition.z)), Vector3.down, out RaycastHit hit, Mathf.Infinity, movementLayer))
        {
            ResetVariables();
            gameObject.transform.position = new Vector3(Mathf.Round(newPosition.x), hit.point.y, Mathf.Round(newPosition.z));
        }
    }

    /// <summary>
    /// Pathfind to new position.
    /// </summary>
    /// <param name="newPosition"> Position to pathfind to. </param>
    public void Move(Vector3 newPosition, float speed = 0.1f)
    {
        if (canCallMove == true)
        {
            if (speed != 0.1f)
            {
                moveSpeed = speed;
            }

            if (Physics.Raycast(new Vector3(Mathf.Round(newPosition.x), newPosition.y, Mathf.Round(newPosition.z)), Vector3.down, out RaycastHit hit, Mathf.Infinity, movementLayer))
            {
                canCallMove = false;
                ResetVariables();
                PathfindingNode newNode = new PathfindingNode(searchRange - 1, new Vector3(Mathf.Round(newPosition.x), hit.point.y, Mathf.Round(newPosition.z)), new Vector3(Mathf.Round(newPosition.x), hit.point.y, Mathf.Round(newPosition.z)), this);
            }
        }
    }

    /// <summary>
    /// Pathfind to new position. This function can be called before the search has been completed. Allowing it to be called more often.
    /// </summary>
    /// <param name="newPosition"> Position to pathfind to. </param>
    public void MoveDynamic(Vector3 newPosition)
    {
        if (Physics.Raycast(new Vector3(Mathf.Round(newPosition.x), newPosition.y, Mathf.Round(newPosition.z)), Vector3.down, out RaycastHit hit, Mathf.Infinity, movementLayer))
        {
            ResetVariables();
            PathfindingNode newNode = new PathfindingNode(searchRange - 1, new Vector3(Mathf.Round(newPosition.x), hit.point.y, Mathf.Round(newPosition.z)), new Vector3(Mathf.Round(newPosition.x), hit.point.y, Mathf.Round(newPosition.z)), this);
            
        }
    }

    /// <summary>
    /// Resets variables to begin path search again
    /// </summary>
    private void ResetVariables()
    {
        generateQueue.Clear();
        navSecondary.Clear();
        pathFound = false;
        canMove = true;
    }

    private void Update()
    {
        if (navMain.ContainsKey(currentPosNode))
        {
            // Moves object toward next node in path
            if (canMove == true)
            {
                gameObject.transform.position = Vector3.MoveTowards(gameObject.transform.position, navMain[currentPosNode].previousPosition, moveSpeed);
            }
            if (lookAtPath == true && pfindingState != PathfindingEnum.stopped)
            {
                // Looks toward next node
                Vector3 direction = (new Vector3(navMain[currentPosNode].previousPosition.x, gameObject.transform.position.y, navMain[currentPosNode].previousPosition.z) - transform.position).normalized; // Direction to move in Normalized
                transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(direction), Time.deltaTime * rotationSmoothing); // Smooth rotation between current rotation and new direction
            }

            // If the object has reached the next node
            if (Vector3.Distance(gameObject.transform.position, navMain[currentPosNode].previousPosition) < 1f && navMain.ContainsKey(currentPosNode))
            {
                // If node position and nodes previous position is the same - reached end of path
                if (currentPosNode == navMain[currentPosNode].previousPosition)
                {
                    //Debug.Log("Reached Position");
                    pfindingState = PathfindingEnum.stopped;
                }
                else
                {
                    // Once reached node, set current node to next node 
                    currentPosNode = navMain[currentPosNode].previousPosition;
                    pfindingState = PathfindingEnum.moving;
                }
            }
        }

        // Generates node from generate queue
        for (int i = 0; i < generateQueue.Count; i++)
        {
            PathfindingNode tempReference = generateQueue.Dequeue();
            tempReference.GenerateNode();
        }

        // Resets can call move, once the path search has been finished
        if (generateQueue.Count < 1 && pathFound == false)
        {
            canCallMove = true;
        }
    }

    private void OnDrawGizmos()
    {
        // Debug view mode
        if (debugMode)
        {
            // Draws green cubes at locations of navMain
            Gizmos.color = Color.green;
            foreach (KeyValuePair<Vector3, PathfindingNode> entry in navMain)
            {
                Gizmos.DrawCube(entry.Key, new Vector3(0.5f, 0.5f, 0.5f));
            }

            // Draws blue cubes at locations of navSecondary
            Gizmos.color = Color.blue;
            foreach (KeyValuePair<Vector3, PathfindingNode> entry in navSecondary)
            {
                Gizmos.DrawCube(new Vector3(entry.Key.x, entry.Key.y + 1, entry.Key.z), new Vector3(0.5f, 0.5f, 0.5f));
            }
        }
    }

    /// <summary>
    /// Pathfinding Node
    /// - Holds nodes position
    /// - Holds the node that created its position
    /// - Checks for walls and edges to create new nodes
    /// </summary>
    private class PathfindingNode
    {
        public Vector3 position; // This nodes position
        public Vector3 previousPosition; // Previous nodes position

        private int positionInRange; // Distance from position
        private Pathfinding pathfinding; // Pathfinding script reference
        private Vector3[] directions = { Vector3.forward, Vector3.right, Vector3.back, Vector3.left }; // Directions to create nodes in

        /// <summary>
        /// Function to create node a position
        /// </summary>
        /// <param name="posInRange">Distance from original position</param>
        /// <param name="prePosition">Reference to the node that created this node</param>
        /// <param name="pos">Position of the new node</param>
        /// <param name="pathfindingRef">Reference to the controller script</param>
        public PathfindingNode(int posInRange, Vector3 prePosition, Vector3 pos, Pathfinding pathfindingRef)
        {
            // Set variables in private class
            positionInRange = posInRange;
            previousPosition = prePosition;
            pathfinding = pathfindingRef;
            position = pos;

            // Add node to generate queue and secondary dictionary
            pathfinding.generateQueue.Enqueue(this);
            pathfinding.navSecondary.Add(position, this);

            // If the search has found needed path
            if (Vector3.Distance(position, pathfinding.gameObject.transform.position) < 1.5f)
            {
                pathfinding.currentPosNode = position;
                pathfinding.pathFound = true;
                pathfinding.canCallMove = true;

                // Replaces navMain with navSecondary
                pathfinding.navMain.Clear();
                foreach (KeyValuePair<Vector3, PathfindingNode> entry in pathfinding.navSecondary)
                {
                    pathfinding.navMain.Add(entry.Key, entry.Value);
                }
                pathfinding.navSecondary.Clear();
            }
        }

        /// <summary>
        /// Check positions around this node to see where more nodes can be created. Then create them if posible.
        /// </summary>
        public void GenerateNode()
        {
            if (pathfinding.pathFound == false && positionInRange != 0)
            {
                // Creates new node in each direction, if there is not a Node, Wall or Edge
                foreach (Vector3 direction in directions)
                {
                    if (!Physics.SphereCast(new Vector3(position.x, position.y + 1f, position.z), 0.5f, direction, out RaycastHit sphereHit, 1, pathfinding.movementLayer))
                    {
                        Vector3 newPosition = position + direction;
                        Vector3 castPosition = new Vector3(newPosition.x, newPosition.y + 1.5f, newPosition.z);

                        // Basic location check to see if there is ground
                        if (Physics.Raycast(castPosition, Vector3.down, out RaycastHit hit, Mathf.Infinity, pathfinding.movementLayer))
                        {
                            newPosition = new Vector3(newPosition.x, hit.point.y, newPosition.z);
                            if (Vector3.Distance(new Vector3(0, position.y, 0), new Vector3(0, newPosition.y, 0)) < 1f)
                            {
                                if (!pathfinding.navSecondary.ContainsKey(newPosition))
                                {
                                    bool wallCheck = true;
                                    foreach (Vector3 wallDirection in directions)
                                    {
                                        // Checks point distance from wall
                                        if (Physics.Raycast(castPosition, wallDirection, out RaycastHit wallHit, pathfinding.objectRadius, pathfinding.movementLayer))
                                        {
                                            wallCheck = false; 
                                            break;
                                        }

                                        // Checks to see if there is a wall or edge in the way
                                        if (Physics.Raycast(castPosition + (wallDirection / 2), Vector3.down, out RaycastHit hit3, Mathf.Infinity, pathfinding.movementLayer))
                                        {
                                            if (Vector3.Distance(new Vector3(0, hit.point.y, 0), new Vector3(0, hit3.point.y, 0)) > 0.25f)
                                            {
                                                wallCheck = false; break;
                                            }
                                        }
                                        else { wallCheck = false; break; }

                                    }

                                    // Checks roof height
                                    if (Physics.Raycast(newPosition, Vector3.up, out RaycastHit hitRoof, pathfinding.objectHeight, pathfinding.movementLayer)) { wallCheck = false; }

                                    // If these is not wall or edge at new location create node
                                    if (wallCheck == true)
                                    {
                                        PathfindingNode newNode = new PathfindingNode(positionInRange - 1, position, newPosition, pathfinding);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
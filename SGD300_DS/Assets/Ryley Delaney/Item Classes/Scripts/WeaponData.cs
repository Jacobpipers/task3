﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// WeaponData is the Component that sits on weapons and holds their data. The weaponAttributes is populated through the 
/// SetWeaponAttributes method.
/// </summary>
public class WeaponData : MonoBehaviour
{
    [SerializeField]
    // store the weapon attributes of this weapon, encapsulated in the WeaponAttributes class.
    private WeaponAttributes weaponAttributes;

    /// <summary>
    /// GetWeaponAttributes returns the current weaponAttributes
    /// </summary>
    public WeaponAttributes GetWeaponAttributes { get { return weaponAttributes; } }

    private void Start()
    {
        if (weaponAttributes.ID != 0)
        {
            weaponAttributes = RyleyJsonReader.GetInstance().GetWeaponByID(weaponAttributes.ID);
        }
    }

    public void SetWeaponAttributes(WeaponAttributes weapon)
    {
        weaponAttributes = weapon;
    }
}

/// <summary>
/// Weapons holds all of the weapon data as retrieved from JSON. This object is instantiated and the allWeaponAttributes array
/// is populated through the constructor's parameter.
/// </summary>
public class Weapons
{
    private WeaponAttributes[] allWeaponAttributes;

    /// <summary>
    /// Constructor for the Weapons class, that takes an array of all weapons represented by WeaponAttributes class.
    /// </summary>
    /// <param name="weaponArray">The array of all weapon data.</param>
    public Weapons(WeaponAttributes[] weaponArray)
    { 
        allWeaponAttributes = weaponArray;
    }

    /// <summary>
    /// GetAllWeapons returns the allWeapons member variable, and logs an error if it doesn't exist.
    /// </summary>
    /// <returns>The allWeapons member variable.</returns>
    public WeaponAttributes[] GetAllWeapons()
    {
        return allWeaponAttributes;
    }

    /// <summary>
    /// GetWeapon will return the weapon based on the provided name. This does not worry about uniqueness, instead it will return
    /// the first object of that name.
    /// </summary>
    /// <param name="weaponName">Name of the weapon being searched for.</param>
    /// <returns>Returns the weapon attributes</returns>
    public WeaponAttributes GetWeaponByName(string weaponName)
    {
        // search all weapons for a weapon of name weaponName
        WeaponAttributes weapon = allWeaponAttributes.ToList().Find(x => x.Name == weaponName);
        return weapon;
    }

    /// <summary>
    /// GetWeapon will return the weapon based on the ID number. This does not worry about uniqueness, instead it will return
    /// the first object of that name.
    /// </summary>
    /// <param name="weaponID"></param>
    /// <returns>Returns the weapon attributes</returns>
    public WeaponAttributes GetWeaponByID(float weaponID)
    {
        // search all weapons for a weapon of ID weaponID
        WeaponAttributes weapon = allWeaponAttributes.ToList().Find(x => x.ID == weaponID);
        return weapon;
    }
}

/// <summary>
/// These variables are written in PascalCase so that when they are represented they are formatted appropriately.
/// </summary>
[System.Serializable]
public class WeaponAttributes //Maps Json data for weapons
{
    public float ID;
    public string Type;
    public string Name;
    public float Damage;
    public float Range;
    public float Speed;
    public float Cost;
}
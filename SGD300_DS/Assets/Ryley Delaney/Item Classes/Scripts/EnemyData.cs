﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyData : MonoBehaviour
{
    [SerializeField]
    //Store the enemy attributes of this NPC, encapsulated in the WeaponAttributes class.
    private EnemyAttributes enemyAttributes;

    /// <summary>
    /// GetEnemyAttributes returns the current enemyAttributes
    /// </summary>
    public EnemyAttributes GetEnemyAttributes { get { return enemyAttributes; } }

    private void Start()
    {
        if (enemyAttributes.ID != -1)
        {
            enemyAttributes = RyleyJsonReader.GetInstance().GetEnemyByID(enemyAttributes.ID);
        }
    }

    public void SetEnemyAttributes(EnemyAttributes enemy)
    {
        enemyAttributes = enemy;
    }
}

///<summary>
/// Enemies holds all of the enemy data as retrieved from JSON. This object is instantiated and the allEnemyAttributes array
/// is populated through the constructor's parameter.
///</summary>
public class Enemies
{
    private EnemyAttributes[] allEnemyAttributes;

    ///<summary>
    ///Constructor for the Enemies class, that takes an array of all enemies represented by the WeaponAttributes class.
    /// </summary>
    /// <param name="enemyArray"> The array of all the weapon data.</param>
    public Enemies(EnemyAttributes[] enemyArray)
    {
        allEnemyAttributes = enemyArray;
    }

    ///<summary>
    /// GetAllEnemies returns the allWeapons member variable, and logs an error if it doesn't exist.
    ///</summary>
    ///<returns>The allEnemies member variable.</returns>
    public EnemyAttributes[] GetAllEnemies()
    {
        return allEnemyAttributes;
    }

    ///<summary>
    /// GetEnemy will return the enemy based on the provided name. This does not worry about uniqueness, instead it will return
    /// the first object of that name.
    /// </summary>
    /// <param name="enemyName">Name of the enemy being searched for.</param>
    /// <returns>Returns the Enemy Attributes</returns>
    public EnemyAttributes GetEnemyByName(string enemyName)
    {
        // Search all weapons for a weapon of name weaponName
        EnemyAttributes enemy = allEnemyAttributes.ToList().Find(x => x.Name == enemyName);
        return enemy;
    }


    ///<summary>
    /// GetEnemyByID will return the enemybsed on the ID number. This does not worry about uniqueness, instead it will return
    /// the first object of that name.
    /// </summary>
    /// <param name="enemyID"></param>
    /// <return>Returns the Enemy Attributes</return>
    public EnemyAttributes GetEnemyByID(float enemyID)
    {
        // search all enemies for a enemy of ID enemyID
        EnemyAttributes enemy = allEnemyAttributes.ToList().Find(x => x.ID == enemyID);
        return enemy;
    }
}
/// <summary>
/// These variables are written in PascalCase so that when they are represented they are formatted appropriately.
/// </summary>
[System.Serializable]
public class EnemyAttributes //Maps Json data for enemies
{
    public float ID;
    public string Type;
    public string Name;
    public float Damage;
    public float Range;
    public float Speed;
    public float Health;

    //public override string ToString()
    //{
    //    return ID.ToString() + " " + Type + " " + Name;
    //}
}

﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

/// <summary>
/// RyleyJsonReader reads from the JSON files and stores the read data as member variables. 
/// RyleyJsonReader is accessed from any other script that requires the information through the
/// singleton access method GetInstance(), whihc will see if the instance singleton exists, and
/// if not will instantiate a new one.
/// </summary>
public class RyleyJsonReader
{
    // singleton with access method
    private static RyleyJsonReader instance;
    public static RyleyJsonReader GetInstance()
    {
        if (instance == null)
        {
            instance = new RyleyJsonReader();
        }

        return instance;
    }

    // member varialbe holding the weapons and enemies
    private Weapons weapons;
    private Enemies enemies;

    /// <summary>
    /// The constructor runs the JSON reading methods and populates our local variables
    /// </summary>
    public RyleyJsonReader()
    {
        ReadWeaponJSON();
        ReadEnemyJSON();
    }

    private void ReadWeaponJSON()
    {
        string weaponpath = Application.dataPath + "/Jed/json/weapons.Json"; // find json - NOTE: change to match file location
        string weaponJsonString = File.ReadAllText(weaponpath); //Reads all weapon json Data
        WeaponAttributes[] weaponList = JsonHelper.FromJson<WeaponAttributes>(weaponJsonString); // Create array from Json
        weapons = new Weapons(weaponList);
        
    }

    private void ReadEnemyJSON()
    {
        string enemypath = Application.dataPath + "/Jed/json/enemies.json"; // find json - NOTE: change to match file location
        string enemyJsonString = File.ReadAllText(enemypath); //Reads all enemy json Data
        Debug.Log(enemyJsonString);
        EnemyAttributes[] enemyList = JsonHelper.FromJson<EnemyAttributes>(enemyJsonString); // Create array from Json

        if (enemyList == null)
        {
            Debug.LogError("EnemyList is null");
            return;
        }

        foreach (EnemyAttributes enemy in enemyList)
        {
            //Debug.Log(enemy.ToString());
            Debug.Log(enemy.Name);
        }
        enemies = new Enemies(enemyList); 
    }

    // ----------------------------------------------------------------------------------------------------------------
    //                              WEAPON METHOD ACCESSORS
    // ----------------------------------------------------------------------------------------------------------------
    public WeaponAttributes GetWeaponByName(string weaponName)
    {
        return weapons.GetWeaponByName(weaponName);
    }

    public WeaponAttributes GetWeaponByID(float weaponID)
    {
        return weapons.GetWeaponByID(weaponID);
    }
    // ----------------------------------------------------------------------------------------------------------------

    // ----------------------------------------------------------------------------------------------------------------
    //                              ENEMY METHOD ACCESSORS
    // ----------------------------------------------------------------------------------------------------------------
    public EnemyAttributes GetEnemyByName(string enemyName)
    {
        return enemies.GetEnemyByName(enemyName);
    }

    public EnemyAttributes GetEnemyByID(float enemyID)
    {
        return enemies.GetEnemyByID(enemyID);
    }
    // ----------------------------------------------------------------------------------------------------------------
}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BirdAI : BaseAIScript
{
    private Vector3 playerPosition; //The position of the player
    private Vector3 flightDirection; //The point in which the bird will fly towards.
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(currentState);
        ChangeState(defaultState);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Debug.Log("player on me");
            ChangeState(AIStates.Wandering);

            if (playerPosition == Vector3.zero )
            {
                playerPosition = other.transform.position;
                Debug.Log(playerPosition);
            }
        }
    }

    // ----------------------------------------------------------------------------------------------------------------
    //                              BASE AI STATE OVERRIDES
    // ----------------------------------------------------------------------------------------------------------------
    protected override void Idle()
    {
        base.Idle();
    }

    protected override void Wandering()
    {
        base.Wandering();

        if (playerPosition != null)
        {
            RotateToTarget();
            flightDirection.x = (transform.position.x + (transform.position.x - playerPosition.x));
            flightDirection.y = (transform.position.y + 4);
            flightDirection.z = transform.position.z + (transform.position.z - playerPosition.z);

            //Debug.Log(flightDirection);

            float step = 9 * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, flightDirection, step);
        }
    }

    /// <summary>
    /// default method for changing states in the enemy class
    /// </summary>
    public void ChangeState(AIStates newState)
    {
        currentState = newState;
    }

    /// <summary>
    /// rotates the enemy to the player
    /// </summary>
    private void RotateToTarget()
    {
        Vector3 direction = (playerPosition - transform.position).normalized;
        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * 60);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class PlayerStateController : MonoBehaviour
{

    public PlayerInfo playerInfo;
    public Animator animator;
    public Collider2D damageBox;

    private AnimStates curState = AnimStates.normal;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            if (curState == AnimStates.normal)
            {
                TakeDamage(1);
            }
        }
       
        if(collision.gameObject.CompareTag("Win"))
        {
            curState = AnimStates.win;
            // Player anim
            // Load Next Level/Menu
            print("Win Hit???");
            
            StartCoroutine(Countdown(4));
        }

    }


    private void OnTriggerStay2D(Collider2D collision)
    {
        
    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Win"))
        {
            curState = AnimStates.normal;
            // Player anim
            // Load Next Level/Menu
            print("Win Out???");
            timeLabel.SetText("");
            StopAllCoroutines();
        }
    }

    public TextMeshProUGUI timeLabel;

    IEnumerator Countdown(int seconds)
    {
        int timer = seconds;
        while (timer > 0)
        {
            timeLabel.SetText(timer.ToString());
            yield return new WaitForSecondsRealtime(1);
            if(timer <= 1)
            {
                if(curState == AnimStates.death)
                    timeLabel.SetText("You Lose");
                else
                    timeLabel.SetText("You Win");
                GetComponent<playerMovement>().enabled = false;
                yield return new WaitForSecondsRealtime(3);
            }
            timer--;
            print(timer);
        }
        if(timer <= 0)
            SceneManager.LoadScene(0);
    }



    public void CheckHealth()
    {
        if (playerInfo.stats.health <= 0)
        {
            GetComponent<playerMovement>().enabled = false;
            curState = AnimStates.death;
            animator.SetTrigger("OnDeath");
            timeLabel.SetText("You Lose");
            StartCoroutine(Countdown(4));
        }
        else
        {
            curState = AnimStates.normal;
        //    damageBox.enabled = true;
        }
        animator.SetBool("done", true);
    }

    public void TakeDamage(int amount)
    {
        curState = AnimStates.damage;

        playerInfo.stats.health -= amount;
        animator.SetBool("done", false);
        animator.SetTrigger("takeDamage");
        
        //  damageBox.enabled = false;
    }
}

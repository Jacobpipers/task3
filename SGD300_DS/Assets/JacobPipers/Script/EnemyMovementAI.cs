﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovementAI : BaseAIScript
{

    public CharacterController2D characterController;
    public float moveSpeed = 40;
    public float stopDistance;

    private float horizontalMove;
    private bool jump = false;
    private bool crouch = false;

    public List<Transform> waypoints = new List<Transform>();

    private Queue<Transform> waypointsQue = new Queue<Transform>();

    private Transform curwaypoint;

    private void Awake()
    {
        currentState = defaultState;
        waypointsQue = new Queue<Transform>(waypoints);
        
    }

    protected override void Hurt()
    {
        base.Hurt();

        horizontalMove = 0;
    }


    protected override void Movement()
    {
        base.Patrolling();


        if (waypoints == null)
            return;


        if (curwaypoint == null || Mathf.Abs(Vector2.Distance(transform.position, curwaypoint.position)) <= stopDistance)
        {
            curwaypoint = waypointsQue.Peek();
            waypointsQue.Enqueue(curwaypoint);

            waypointsQue.Dequeue();
        }

        var forward = transform.TransformDirection(Vector3.left);
        var toOther = curwaypoint.position - transform.position;

        var dot =   Vector3.Dot(forward.normalized, toOther.normalized);

        var direction = -dot;

        if (dot < 0)
        {
            direction = 1;
        } else
        {
            direction = -1;
        }



  //      print("Direction: " + direction);

        horizontalMove = moveSpeed * direction;

    }


    private void FixedUpdate()
    {
        // Move our character
        characterController.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;

    }

}

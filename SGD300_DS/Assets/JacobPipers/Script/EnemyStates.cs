﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStates : MonoBehaviour
{

    public PlayerInfo enemyInfo;
    public Animator animator;
    public Collider2D damageBox;

    public BaseAIScript AIState;

    private AIStates oldState;

    private AnimStates curState = AnimStates.normal;

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.GetComponent<CharacterController2D>() != null && collision.gameObject.CompareTag("Player"))
        {
            if(curState == AnimStates.normal)
            {
                TakeDamage(1);
            }
        }
    }

    public void CheckHealth()
    {
        if (enemyInfo.stats.health <= 0)
        {
            curState = AnimStates.damage;
            animator.SetTrigger("OnDeath");
        } else
        {
            curState = AnimStates.normal;
            damageBox.enabled = true;
        }
        AIState.SetCurrentState = oldState;
    }

    public void TakeDamage(int amount)
    {
        oldState = AIState.GetCurrentState;
        AIState.SetCurrentState = AIStates.Hurt;
        

        curState = AnimStates.damage;
      
        enemyInfo.stats.health -= amount;
        animator.SetBool("takeDamage", true);
        damageBox.enabled = false;
    }
}


public enum AnimStates
{
    normal,
    damage,
    death,
    win
}

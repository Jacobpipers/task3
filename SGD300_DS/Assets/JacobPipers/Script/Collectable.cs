﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectable : MonoBehaviour
{
    public LayerMask mask;
    public CollectableType collectable;

    public Animator animator;

    private bool isCollected = false;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isCollected)
            return;

        var playerInfo = collision.gameObject.GetComponent<PlayerInfo>();
        var characterInv = collision.gameObject.GetComponent<CharactersInventoreyInfo>();
        if (playerInfo != null && collision.CompareTag("Player"))
        {
           
            if (!isCollected)
            {
                isCollected = true;
                GetComponent<CircleCollider2D>().enabled = false;
                switch (collectable)
                {

                    case CollectableType.Cherry:
                        print(collision.name);
                        animator.SetTrigger("Collected");
                        // playerInfo.stats.health++;
                        //  Mathf.Clamp(playerInfo.stats.health, 0, playerInfo.stats.mHealth);
                        var newItem = new ItemClass();
                        ItemClass.SetItemValues(newItem, 16, 0, 1);
                        characterInv.invRef.AddItemToInv(newItem);
                        break;

                    case CollectableType.Gem:
                        print(collision.name);
                        animator.SetTrigger("Collected");
                        var tempItem = new ItemClass();
                        ItemClass.SetItemValues(tempItem, 0, 0, 1);
                        characterInv.invRef.AddItemToInv(tempItem);
                        playerInfo.curGold++;
                        break;
                }
            }
           

         
        }
    }
}


public enum CollectableType
{
    Cherry,
    Gem
}
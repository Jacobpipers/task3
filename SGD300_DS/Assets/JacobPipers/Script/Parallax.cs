﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public Transform camera;
    public float speedCoefficient;
    Vector3 lastpos;

    void Start()
    {
        if (camera == null)
            camera = Camera.main.transform;
        lastpos = camera.position;
       
    }

   

    void Update()
    {
        if (camera == null)
            return;

        transform.position -= ((lastpos - camera.position) * speedCoefficient);
        lastpos = camera.position;
    }
}

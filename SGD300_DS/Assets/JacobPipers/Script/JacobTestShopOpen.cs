﻿/// <summarey>
/// This was just a quick script to allow testing of the shop.
/// Created By Jacob Pipers
/// </summarey>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JacobTestShopOpen : MonoBehaviour
{

    // The Distance can Interact with the object.
    public int interActDistance = 5;


    void OnMouseOver()
    {
        //If your mouse hovers over the GameObject with the script attached, output this message
        print("OnMouse OVer");
       
    }

    void OnMouseExit()
    {
        //The mouse is no longer hovering over the GameObject so output this message each frame
    }


    void OnMouseDown()
    {
        GetComponent<IInteractable>().Interact();
    }
}

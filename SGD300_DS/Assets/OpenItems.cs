﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenItems : MonoBehaviour
{

    public CanvasGroup canv;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.I))
        {
            if(canv.interactable)
            {
                Debug.LogWarning("Open??");
                GetComponent<Button>().onClick.Invoke();
            }
        }
    }
}

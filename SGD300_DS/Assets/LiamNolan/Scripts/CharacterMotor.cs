﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

/// <summary>
/// TODO
/// </summary>
public class CharacterMotor : MonoBehaviour
{
    private Pathfinding pathfinding;

    void Start()
    {
        pathfinding = GetComponent<Pathfinding>();
    }

    /// <summary>
    /// CH - think about how the player input will pass the information into here, you will need a parameter
    /// 
    /// Move to selected space takes a raycast from the camera position to the location 
    /// clicked on the screen, saves the information as a gameobject. This also allows the
    /// player to move using the pathfinder script.
    /// </summary>
    /// <returns>TODO</returns>
    public void MoveToSelectedSpace(Vector3 moveToPosition)
    {
        pathfinding.Move(new Vector3(moveToPosition.x, moveToPosition.y + 2, moveToPosition.z));
    }

    public bool IsMoving()
    {
        if (pathfinding.pathfindingState == Pathfinding.PathfindingEnum.moving) {
            return true;
        } else {
            return false;
        }
    }
}

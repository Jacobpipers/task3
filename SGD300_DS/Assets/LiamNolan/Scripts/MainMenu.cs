﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

    public void NewGame ()
    {
        // Add scene in the build settings menu within Unity
        Debug.Log("New Game Loading!");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // code area for load Game
    public void LoadGame ()
    {
        Debug.Log("Loading previous save");
    }

    // Exits the game when the button is pressed
    public void ExitGame ()
    {
        Debug.Log("Exiting the game!");
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
      Application.Quit();
#endif
    }

}

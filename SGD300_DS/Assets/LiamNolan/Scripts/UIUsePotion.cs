﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///  Written by : Liam Nolan or LDN001
///  email: ldn001@student.usc.edu.au
///  
/// this script handles 
/// </summary>

public class UIUsePotion : MonoBehaviour
{
    public PlayerController playerController;
   

    // Code Clean
    private void CheckPartyAndApplyPotion(int type)
    {
        var party = playerController.PartyMembers;
        foreach (var member in party)
        {
            CharactersInventoreyInfo charactersInventoreyInfo = member.GetComponent<CharactersInventoreyInfo>();
            // Assign inventorey to the character.
            var inv = charactersInventoreyInfo.invRef;
            SlotScript[] slotScripts = inv.GetComponentsInChildren<SlotScript>();
            foreach (var slot in slotScripts)
            {
                if (slot.GetComponent<SlotScript>() != null && slot.GetComponent<SlotScript>().slotInfo.storedItemObject != null && slot.GetComponent<SlotScript>().slotInfo.storedItemObject.GetComponent<ItemScript>() != null)
                {
                    var item = slot.GetComponent<SlotScript>().slotInfo.storedItemObject.GetComponent<ItemScript>().item;
                    Debug.LogWarning("Use Potion Start: " + item.CategoryID.ToString());
                    if (item.CategoryID == (int)ItemType.Other)
                    {
                        if (item.TypeID == type)
                        {
                            Debug.LogWarning("Use Potion");
                            inv.PotionHeal(item, slot.gameObject);
                            break;
                        }
                    }
                }
                
            }
        }
    }


    public void HealthUsePotion() {

        CheckPartyAndApplyPotion(0);

    }

    public void ManaUsePotion()
    {
        CheckPartyAndApplyPotion(1);
    }
}

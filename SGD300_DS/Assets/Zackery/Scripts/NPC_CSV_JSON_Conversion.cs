﻿//Author Dock from 'Unify Community' 10-Jan-2012 [http://wiki.unity3d.com/index.php?title=CSVReader&fbclid=IwAR0tDaOI1OpGKjItw07kASnQprksyk6MNt4swuNH8EMeIJ7BpwBDLla1PcE]
//Author Zackery Camille 28-Aug-2019
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;

public class NPC_CSV_JSON_Conversion : MonoBehaviour
{
    // The 2D string array for the csv file.
    private string[,] csvGrid;

    // The string for the json file for before a new file is created.
    public string newJson;

    //Assign the csv file ('Item_List') as a TextAsset type.
    public TextAsset npcList;

    //Outputs the content of a 2D array, useful for checking the importer.
    static public void DebugOutputGrid(string[,] grid)
    {
        string textOutput = "";
        for (int y = 0; y < grid.GetUpperBound(1); y++)
        {
            for (int x = 0; x < grid.GetUpperBound(0); x++)
            {
                textOutput += grid[x, y];
                textOutput += "|";
            }

            textOutput += "\n";
        }
        Debug.Log(textOutput);
    }

    private ListOfNPCs GenerateJson()
    {
        // Create the class that holds the list of items
        ListOfNPCs newJson = new ListOfNPCs();

        //The following for loop reads through each line of the csv,
        //skipping the column headers and assigns each cell of data,
        //to the corresponding column header.
        for (int i = 1; i < csvGrid.GetLength(1); i++)
        {
            //An item is created only if the line has a uid.
            if (csvGrid[0, i] != null)
            {
                NPCDataTypeList newNPC = new NPCDataTypeList(); // Creates new item
                newNPC.npcStats.globalID = csvGrid[0, i];
                newNPC.npcStats.affiliation = csvGrid[1, i];
                newNPC.npcStats.name = csvGrid[2, i];
                newNPC.npcStats.archetype = csvGrid[3, i];
                newNPC.npcStats.health = csvGrid[4, i];
                newNPC.npcStats.attackRange = csvGrid[5, i];
                newNPC.npcStats.damage = csvGrid[6, i];
                newNPC.npcStats.minDamage = csvGrid[7, i];
                newNPC.npcStats.maxDamage = csvGrid[8, i];
                newNPC.npcStats.armour = csvGrid[9, i];
                newNPC.npcStats.exp = csvGrid[10, i];
                // Adds the new item to the item list.
                newJson.npcs.Add(newNPC);
            }
        }
        // Returns new list of items.
        return newJson;
    }

    //Splits a CSV file into a 2D string array.
    static public string[,] SplitCSVGrid(string csvNPCListText)
    {
        string[] lines = csvNPCListText.Split("\n"[0]);

        //finds the max width of row.
        int width = 0;

        for (int i = 0; i < lines.Length; i++)
        {
            string[] row = SplitCSVLine(lines[i]);
            width = Mathf.Max(width, row.Length);
        }

        //Creates new 2D string grid to output to.
        string[,] outputGrid = new string[width + 1, lines.Length + 1];

        for (int y = 0; y < lines.Length; y++)
        {
            string[] row = SplitCSVLine(lines[y]);

            for (int x = 0; x < row.Length; x++)
            {
                outputGrid[x, y] = row[x];

                //The following line of code is to replace "" with " in the output. 
                outputGrid[x, y] = outputGrid[x, y].Replace("\"\"", "\"");
            }
        }
        return outputGrid;
    }

    //Splits a CSV row.
    static public string[] SplitCSVLine(string line)
    {
        return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
        @"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",
        System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
                select m.Groups[1].Value).ToArray();
    }

    // Start is called before the first frame update.
    private void Start()
    {
        // Spilts the csv file into a 2D array "csvGrid".
        csvGrid = SplitCSVGrid(npcList.ToString());

        // Path that it will save the new json file.
        string path = "Assets/Zackery/Scripts/npcJSON.json";

        // Generate json string to be added to new json file.
        string str = JsonUtility.ToJson(GenerateJson());

        using (FileStream fs = new FileStream(path, FileMode.Create))
        {
            using (StreamWriter writer = new StreamWriter(fs))
            {
                writer.Write(str);
            }
        }

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.Refresh(); // Refresh the unity UI to see new file in folder
#endif

        //The following line below splits the string data from the csv where there is the comma character ',' so that we can read through the data in the csv.
        string[,] grid = SplitCSVGrid(npcList.text);

        Debug.Log("size = " + (1 + grid.GetUpperBound(0)) + "," + (1 + grid.GetUpperBound(1)));

        //Calls the DebugOutputGrid Method
        DebugOutputGrid(grid);
    }
}

[System.Serializable]
public class NPCDataType
{
    //This class holds all the item columns pertaining to the csv. 
    //Item columns are listed inside this class and are assigned a data type.
    public string globalID;
    public string affiliation;
    public string name;
    public string archetype;
    public string health;
    public string attackRange;
    public string damage;
    public string minDamage;
    public string maxDamage;
    public string speed;
    public string armour;
    public string exp;
}

[System.Serializable]
public class NPCDataTypeList
{
    //Sets the items column heading used in the json file to 'itemStats'.
    public NPCDataType npcStats = new NPCDataType();
}

[System.Serializable]
public class ListOfNPCs
{
    //The following list is initialised to later store the data from the csv to then be parsed into the json file.
    public List<NPCDataTypeList> npcs = new List<NPCDataTypeList>();
}
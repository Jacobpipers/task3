﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class NPC_JSON_Reader : MonoBehaviour
{
    private NPCCollection[] allNPCs;

    string npcJSONString;
    string npcPath;

    public NPCCollection[] GetAllNPCs()
    {
        if (allNPCs != null)
        {
            return allNPCs;
        }
        else
        {
            Debug.LogError("There are no NPCs.");
            return null;
        }
    }

    //Start is called before the first frame update.
    void Start()
    {
        //Find json - NOTE: change to match file location.
        npcPath = Application.dataPath + "/Zackery/Scripts/npcJSON.json";

        //Reads all weapon json Data.
        npcJSONString = File.ReadAllText(npcPath);

        //Create array from Json.
        NPCCollection[] npcList = NPCJSONHelper.FromJson<NPCCollection>(npcJSONString);
        allNPCs = npcList;

        Debug.Log(npcJSONString);
    }

    /// <summary>
    /// GetAllItems returns the allItems member variable, and logs an error if it doesn't exist.
    /// </summary>
    /// <returns>The allItems member variable.</returns>

}

[System.Serializable]
//Maps JSON data for Items.
public class NPCCollection
{
    public string globalID;
    public string affiliation;
    public string name;
    public string archetype;
    public string health;
    public string attackRange;
    public string damage;
    public string minDamage;
    public string maxDamage;
    public string speed;
    public string armour;
    public string exp;
}

public static class NPCJSONHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.items;
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);

    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] items;
    }
}
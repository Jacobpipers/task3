﻿/// <summary>
///     Written By: Keanu Brown-Duthie
///     Email: keb026@student.usc.edu.au
///     
///     This Script contains the enum for the AI states
/// </summary>

public enum AIStates
{
    Movement,
    Patrolling,
    Combat,
    Idle,
    Wandering,
    Aggressive,
    Passive,
    Defensive,
    Hurt
}
﻿/// <summary>
///     Written By: Keanu Brown-Duthie
///     Email: keb026@student.usc.edu.au
///     
///     This Script contains the enums for the movement and targeting orders
/// </summary>

public enum MovementOrder
{
    MoveFreely,
    Engage,
    HoldGround
}

public enum TargetingOrder
{
    TargetClosest,
    TargetWeakest,
    TargetStrongest
}

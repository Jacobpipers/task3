﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     Written By: Keanu Brown-Duthie
///     Email: keb026@student.usc.edu.au
///     
///     This Script contains the structs required for prop animation
/// </summary>

[System.Serializable]
public struct ParentAnimationComponent
{
    [Tooltip(
        "Set the amount of times you want the animation to loop here. If set to 0 " +
        "the animation will loop indefinitely."
    )]
    public int loopAmount;
    [Tooltip("Setup the animation components for the parent here.")]
    public AnimationComponent[] animation;

    [HideInInspector]
    public float totalDuration;
}

[System.Serializable]
public struct ChildAnimationComponent
{
    [Tooltip("Add any child objects that you wish to affect with the animation here.")]
    public GameObject[] childComponents;
    [Tooltip(
        "Set the amount of times you want the animation to loop here. If set to 0 " +
        "the animation will loop indefinitely."
    )]
    public int loopAmount;
    [Tooltip("Setup the animation components for the children here.")]
    public AnimationComponent[] animation;

    [HideInInspector]
    public float totalDuration;
}

[System.Serializable]
public struct AnimationComponent
{
    [Header("Time")]
    [Tooltip("Set the amount of seconds that you wish this animation component to run for here.")]
    public float duration;

    [Header("Rotation")]
    [Tooltip(
        "Set the amount of x-axis rotation you wish to add to the affected component/s " +
        "throughout the duration of this animation component."
    )]
    public float xRotation;
    [Tooltip(
        "Set the amount of y-axis rotation you wish to add to the affected component/s " +
        "throughout the duration of this animation component."
    )]
    public float yRotation;
    [Tooltip(
        "Set the amount of z-axis rotation you wish to add to the affected component/s " +
        "throughout the duration of this animation component."
    )]
    public float zRotation;
    [Tooltip("Set the spacial type to use for the rotation here.")]
    public Space rotationSpaceHandle;
    [Tooltip(
        "Set the object to rotate around here. Dont't set if you don't want the " +
        "affected component/s to rotate around an object."
    )]
    public GameObject objectReference;

    [Header("Movement")]
    [Tooltip(
        "Set the amount of x-axis movement you wish to add to the effected component/s " +
        "throughout the duration of this animation component."
    )]
    public float xMovement;
    [Tooltip(
        "Set the amount of y-axis movement you wish to add to the effected component/s " +
        "throughout the duration of this animation component."
    )]
    public float yMovement;
    [Tooltip(
        "Set the amount of z-axis movement you wish to add to the effected component/s " +
        "throughout the duration of this animation component."
    )]
    public float zMovement;
    [Tooltip("Set the spacial type to use for the movement here.")]
    public Space movementSpaceHandle;
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     Written By: Keanu Brown-Duthie
///     Email: keb026@student.usc.edu.au
///     
///     This script holds three different methods that are used to fetch a new target
///     
///     TargetingOrderHelpers
///     - GetClosestEnemy(Collider[] enemies, Vector3 playerLocation)
///     - GetWeakestEnemy(Collider[] enemies)
///     - GetStrongestEnemy(Collider[] enemies)
/// </summary>
public static class TargetingOrderHelpers
{
    /// <summary>
    ///     Gets the closest enemy and return it as the new target
    /// </summary>
    /// <param name="enemies"> The enemies within range of the player </param>
    /// <param name="playerLocation"> The players current location </param>
    /// <returns> Returns the closest enemy as the new target </returns>
    public static GameObject GetClosestEnemy(Collider[] enemies, Vector3 playerLocation)
    {
        GameObject closestTarget = enemies[0].gameObject;

        foreach (Collider enemy in enemies)
        {
            float closestDistance = Vector3.Distance(playerLocation, closestTarget.transform.position);
            float enemyDistance = Vector3.Distance(playerLocation, enemy.gameObject.transform.position);

            if (enemyDistance < closestDistance)
            {
                closestTarget = enemy.gameObject;
            }
        }

        return closestTarget;
    }

    /// <summary>
    ///     Gets the weakest enemy and return it as the new target
    /// </summary>
    /// <param name="enemies"> The enemies within range of the player </param>
    /// <returns> Returns the weakest enemy as the new target </returns>
    public static GameObject GetWeakestEnemy(Collider[] enemies)
    {
        GameObject weakestTarget = enemies[0].gameObject;

        foreach (Collider enemy in enemies)
        {
            // todo: add proper weakness check
            float weakestAmount = float.Parse(weakestTarget.name);
            float enemyAmount = float.Parse(enemy.name);

            if (weakestAmount > enemyAmount)
            {
                weakestTarget = enemy.gameObject;
            }
        }

        return weakestTarget;
    }

    /// <summary>
    ///     Gets the stongest enemy and return it as the new target
    /// </summary>
    /// <param name="enemies"> The enemies within range of the player </param>
    /// <returns> Returns the strongest enemy as the new target </returns>
    public static GameObject GetStrongestEnemy(Collider[] enemies)
    {
        GameObject strongestTarget = enemies[0].gameObject;

        foreach (Collider enemy in enemies)
        {
            // todo: add proper strength check
            float strongestAmount = float.Parse(strongestTarget.name);
            float enemyAmount = float.Parse(enemy.name);

            if (strongestAmount < enemyAmount)
            {
                strongestTarget = enemy.gameObject;
            }
        }

        return strongestTarget;
    }
}

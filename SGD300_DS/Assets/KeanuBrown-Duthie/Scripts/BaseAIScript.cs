﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     Written By: Keanu Brown-Duthie
///     Email: keb026@student.usc.edu.au
///     
///     This script holds the base AI functionality
///     Scripts such as the player AI and the enemy AI would inherit from this script
///     
///     BaseAIScript
///     - Awake()
///     - Update()
///     - Idle()
///     - Movement()
///     - Patrolling()
///     - Wandering()
///     - Combat()
///     - Aggresive()
///     - Passive()
///     - Defensive()
/// </summary>
public class BaseAIScript : MonoBehaviour
{
    [Header("Default State Setup")]
    [SerializeField]
    [Tooltip("Set the default AI state here.")]
    protected AIStates defaultState = AIStates.Idle;

    // Protected variables
    protected AIStates currentState; // Current state of the AI

    // Getters/Setters
    public AIStates GetCurrentState { get { return currentState; } } // Getter for the current state
    public AIStates SetCurrentState { set { currentState = value; } } // Getter for the current state


    /// <summary>
    ///     Sets the AI to it's starting state before Start() or Update() are called
    /// </summary>
    private void Awake()
    {
        currentState = defaultState;
    }

    /// <summary>
    ///     Handles the current AI states behaviour once every frame
    /// </summary>
    void Update()
    {
        switch (currentState)
        {
            // Switch to Idle State
            case AIStates.Idle:
                Idle();
                break;
            // Switch to Movement State
            case AIStates.Movement:
                Movement();
                break;
            // Switch to Patrolling State
            case AIStates.Patrolling:
                Patrolling();
                break;
            // Switch to Wandering State
            case AIStates.Wandering:
                Wandering();
                break;
            // Switch to Combat State
            case AIStates.Combat:
                Combat();
                break;
            // Switch to Aggressive State
            case AIStates.Aggressive:
                Aggressive();
                break;
            // Switch to Passive State
            case AIStates.Passive:
                Passive();
                break;
            // Switch to Defensive State
            case AIStates.Defensive:
                Defensive();
                break;
            case AIStates.Hurt:
                Hurt();
                break;

        }
    }


    protected virtual void Hurt()
    {
      //  Debug.Log("Hurt");
    }

    /// <summary>
    ///     Switches the current state of the AI to Idle
    /// </summary>
    protected virtual void Idle()
    {
       // Debug.Log("Idle");
    }

    /// <summary>
    ///     Switches the current state of the AI to Movement
    /// </summary>
    protected virtual void Movement()
    {
      //  Debug.Log("Movement");
    }

    /// <summary>
    ///     Switches the current state of the AI to Patrolling
    /// </summary>
    protected virtual void Patrolling()
    {
      //  Debug.Log("Patrolling");
    }

    /// <summary>
    ///     Switches the current state of the AI to Wandering
    /// </summary>
    protected virtual void Wandering()
    {
     //   Debug.Log("Wandering");
    }

    /// <summary>
    ///     Switches the current state of the AI to Combat
    /// </summary>
    protected virtual void Combat()
    {
     //   Debug.Log("Combat");
    }

    /// <summary>
    ///     Switches the current state of the AI to Aggressive
    /// </summary>
    protected virtual void Aggressive()
    {
     //   Debug.Log("Aggressive");
    }

    /// <summary>
    ///     Switches the current state of the AI to Passive
    /// </summary>
    protected virtual void Passive()
    {
    //    Debug.Log("Passive");
    }

    /// <summary>
    ///     Switches the current state of the AI to Defensive
    /// </summary>
    protected virtual void Defensive()
    {
     //   Debug.Log("Defensive");
    }
}

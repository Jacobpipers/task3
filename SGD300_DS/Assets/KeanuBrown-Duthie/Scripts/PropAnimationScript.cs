﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
///     Written By: Keanu Brown-Duthie
///     Email: keb026@student.usc.edu.au
///     
///     This script applies rotation and movement to animate an object and it's children
///     
///     PropAnimationScript
///     - Awake()
///     - Update()
///     - CalculateTotalDuration(AnimationComponent[] animationComponents)
///     - HandleAnimationComponent(GameObject propObject, AnimationComponent[] animComps, float totalDuration, int loopAmount)
///     - HandleAnimation(GameObject propObject, AnimationComponent[] animComps, float startTime, float endTime)
/// </summary>
public class PropAnimationScript : MonoBehaviour
{
    [Header("Animation Setup")]
    [SerializeField]
    [Tooltip("Setup the parents animation here.")]
    private ParentAnimationComponent parentAnimation;
    [SerializeField]
    [Tooltip("Setup the childrens animation here.")]
    private ChildAnimationComponent[] childAnimation;

    // Private variables
    private float animationTimer; // The current time that has passed while running the animation/s
    private float prevAnimationTimer; // The previous state of the animation timer

    /// <summary>
    ///     Sets the total durations for each set of animations before Start() or Update() are called
    /// </summary>
    private void Awake()
    {
        // Set total duration for parent animation
        parentAnimation.totalDuration = CalculateTotalDuration(parentAnimation.animation);

        // Set total duration for all child animations
        for (int i = 0; i < childAnimation.Length; i++)
        {
            childAnimation[i].totalDuration = CalculateTotalDuration(childAnimation[i].animation);
        }
    }

    /// <summary>
    ///     Updates animation/s once every frame
    /// </summary>
    private void Update()
    {
        prevAnimationTimer = animationTimer;
        animationTimer += Time.deltaTime;

        // Handle parent animation
        HandleAnimationComponent(
            this.gameObject,
            parentAnimation.animation,
            parentAnimation.totalDuration,
            parentAnimation.loopAmount
        );

        // Handle child animation
        foreach (ChildAnimationComponent childAnimComp in childAnimation)
        {
            foreach (GameObject propObject in childAnimComp.childComponents)
            {
                HandleAnimationComponent(
                    propObject,
                    childAnimComp.animation,
                    childAnimComp.totalDuration,
                    childAnimComp.loopAmount
                );
            }
        }
    }

    /// <summary>
    ///     Calculates the total duration for an animation
    /// </summary>
    /// <param name="animationComponents"> The animation components for a child or a parent </param>
    /// <returns> Returns the total duration for the animation components passed through </returns>
    private float CalculateTotalDuration(AnimationComponent[] animationComponents)
    {
        float totalDuration = 0.0f;
        foreach (AnimationComponent animComp in animationComponents)
        {
            totalDuration += animComp.duration;
        }

        return totalDuration;
    }

    /// <summary>
    ///     Sets up the loops that an animation will run in and executes them
    /// </summary>
    /// <param name="propObject"> The object to apply the animation to </param>
    /// <param name="animComps"> The animation components to use when animating </param>
    /// <param name="totalDuration"> The total duration of one animation cycle </param>
    /// <param name="loopAmount"> The amount of times that the animation can loop </param>
    private void HandleAnimationComponent(GameObject propObject, AnimationComponent[] animComps, float totalDuration, int loopAmount)
    {
        float prevTimeLeft = prevAnimationTimer / totalDuration;
        float currentTimeLeft = animationTimer / totalDuration;
        float loopStart = prevAnimationTimer % totalDuration;

        // Finish off previous animation loop if loop overlapping
        if (Mathf.FloorToInt(prevTimeLeft) < Mathf.FloorToInt(currentTimeLeft))
        {
            HandleAnimation(propObject, animComps, loopStart, totalDuration);
            loopStart = 0.0f;
        }

        // New/Continue animation Loop
        if (prevTimeLeft <= loopAmount || loopAmount == 0)
        {
            HandleAnimation(propObject, animComps, loopStart, animationTimer % totalDuration);
        }
    }

    /// <summary>
    ///     Plays a segment of the animation
    /// </summary>
    /// <param name="propObject"> The object to apply the animation to </param>
    /// <param name="animComps"> The animation components to use when animating </param>
    /// <param name="startTime"> The start time for the segment of the animation to play </param>
    /// <param name="endTime"> The end time for the segment of the animation to play </param>
    private void HandleAnimation(GameObject propObject, AnimationComponent[] animComps, float startTime, float endTime)
    {
        float timeCounter = 0.0f;
        foreach (AnimationComponent animComponent in animComps)
        {
            float prevTimeCounter = timeCounter;
            timeCounter += animComponent.duration;

            if (prevTimeCounter < endTime && timeCounter > startTime)
            {
                float durationLeft = animComponent.duration;
                if (prevTimeCounter < startTime)
                {
                    durationLeft -= (startTime - prevTimeCounter);
                }

                if (timeCounter > endTime)
                {
                    durationLeft -= (timeCounter - endTime);
                }

                if (durationLeft > 0.0f)
                {
                    // Handle animation rotation
                    Vector3 rotationToAdd = new Vector3(
                        animComponent.xRotation * durationLeft,
                        animComponent.yRotation * durationLeft,
                        animComponent.zRotation * durationLeft
                    );

                    if (animComponent.objectReference)
                    {
                        propObject.transform.RotateAround(
                            animComponent.objectReference.transform.position,
                            rotationToAdd,
                            1.0f
                        );
                    }
                    else
                    {
                        propObject.transform.Rotate(rotationToAdd, animComponent.rotationSpaceHandle);
                    }

                    // Handle animation movement
                    Vector3 movementToAdd = new Vector3(
                        animComponent.xMovement * durationLeft,
                        animComponent.yMovement * durationLeft,
                        animComponent.zMovement * durationLeft
                    );

                    propObject.transform.Translate(movementToAdd, animComponent.movementSpaceHandle);
                }
            }

            if (timeCounter > endTime)
            {
                break;
            }
        }
    }
}

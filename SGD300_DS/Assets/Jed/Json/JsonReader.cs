﻿/*using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class JsonReader : MonoBehaviour
{
    
    string weaponpath;
    string enemypath;
    string weaponJsonString;
    string enemyJsonString;

    private Weapons[] allWeapons;

    // Start is called before the first frame update
    void Start()
    {
        weaponpath = Application.dataPath + "/Jed/json/weapons.Json"; // find json - NOTE: change to match file location
        weaponJsonString = File.ReadAllText(weaponpath); //Reads all weapon json Data

        enemypath = Application.dataPath + "/Ryley Delaney/Enemy spawn/Scripts/Enemies_jsonData.json"; // find json - NOTE: change to match file location
        enemyJsonString = File.ReadAllText(enemypath); //Reads all enemy json Data

        Weapons[] weaponList = JsonHelper.FromJson<Weapons>(weaponJsonString); // Create array from Json
        allWeapons = weaponList;
        Enemies[] enemyList = JsonHelper.FromJson<Enemies>(enemyJsonString); // Create array from Json

        //Debug.Log(weaponList[0].Name);
        //Debug.Log(enemyList[0].Name);

        Debug.Log(weaponJsonString); // Display json is being called correctly
        Debug.Log(enemyJsonString); // Display json is being called correctly

        // Series of debugs showing that we can call specific values
        Weapons Sword = JsonUtility.FromJson<Weapons>(weaponJsonString);
        Debug.Log("Sword damage is equal to " + weaponList[0].Damage);

        Weapons Club = JsonUtility.FromJson<Weapons>(weaponJsonString);
        Debug.Log("Yo it's a " + weaponList[1].Name);

        Weapons ShortBow = JsonUtility.FromJson<Weapons>(weaponJsonString);
        Debug.Log("this shortbow has an attack speed of " + weaponList[2].Speed);

        Weapons LongBow = JsonUtility.FromJson<Weapons>(weaponJsonString);
        Debug.Log("This longbow has a range of " + weaponList[3].Range);

        Weapons Fire = JsonUtility.FromJson<Weapons>(weaponJsonString);
        Debug.Log("Fire costs " + weaponList[4].Cost + " mana to cast");

        Weapons Lightning = JsonUtility.FromJson<Weapons>(weaponJsonString);
        Debug.Log("wowsers it's a " + weaponList[5].Name + " spell!");

    }

    /// <summary>
    /// GetAllWeapons returns the allWeapons member variable, and logs an error if it doesn't exist.
    /// </summary>
    /// <returns>The allWeapons member variable.</returns>
    public Weapons[] GetAllWeapons()
    {
        if (allWeapons != null)
        {
            return allWeapons;
        }
        else
        {
            Debug.LogError("We have no weapons");
            return null;
        }
    }
}

[System.Serializable]
public class Weapons //Maps Json data for weapons
{
    public float ID;
    public string Type;
    public string Name;
    public float Damage;
    public float Range;
    public float Speed;
    public float Cost;
}

[System.Serializable]
public class Enemies //Maps Json data for enemies
{
    public float ID;
    public string Type;
    public string Name;
    public float Damage;
    public float Range;
    public float Speed;
    public float Health;
}

public static class JsonHelper
{
    public static T[] FromJson<T>(string json)
    {
        Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json);
        return wrapper.Items;
    }

    public static string ToJson<T>(T[] array)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper);
    }

    public static string ToJson<T>(T[] array, bool prettyPrint)
    {
        Wrapper<T> wrapper = new Wrapper<T>();
        wrapper.Items = array;
        return JsonUtility.ToJson(wrapper, prettyPrint);
    }

    [Serializable]
    private class Wrapper<T>
    {
        public T[] Items;
    }
    
}
*/
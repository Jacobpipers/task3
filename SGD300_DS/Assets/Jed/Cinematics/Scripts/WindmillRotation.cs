﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindmillRotation : MonoBehaviour
{
    int rotationsPerMinute = 20; // set rotation speed

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, 6 * rotationsPerMinute * Time.deltaTime); // turns our windmill correctly
    }
}
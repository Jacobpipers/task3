﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnToMenu : MonoBehaviour
{
    public float cutsceneLength = 10; //Set to appropriate cutscene length
    float cutsceneTimer; //Stores delta time
    
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        cutsceneTimer += Time.deltaTime; //cutsceneTimer matches deltaTime

        if(cutsceneTimer >= cutsceneLength) //If delta time runs over the set cutscene length; return to the main menu
        {
            Debug.Log("Cutscene has ended, returning to menu");
            Application.LoadLevel("MainMenuUITest"); //Use Main menu level name instead in final version
        }
    }
}
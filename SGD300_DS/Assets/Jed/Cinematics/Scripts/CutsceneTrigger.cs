﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutsceneTrigger : MonoBehaviour
{
    public GameObject playCutscene; // Set which timeline/ cutscene to use in editor

    // Run if an object with a collider enters the trigger
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player")) // Only play cutscene if the game object has the "Player" tag
        {
            Debug.Log("Cutscene trigger entered");
            playCutscene.SetActive(true); //Activate our cutscene
        }
    }
}
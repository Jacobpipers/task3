﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sets the position of the referenced game object (Camera) with an offset
/// according to the position of the referenced transform (Active Player).
/// Rotates the referenced game object (Camera) according to the mouse X and
/// Y inputs when the middle-mouse button is held down.
/// The 'tilt' is clamped to min 40 and max 70.
/// Made by Ruan Vermaak.
/// Edited by CH 16/08/19
/// </summary>

public class PlayerCamera : MonoBehaviour
{
    [Header("Inspector Populated References")]
    //Main camera variable visible in editor
    [SerializeField] Camera mainCamera;

    // The game object for the camera to follow and target
    [SerializeField] Transform activePlayer;

    [Header("Camera Controls")]
    // Sensitivity level for the camera rotation
    [SerializeField] private float camSensitivity = 4.0f;
    [SerializeField] private float minDistance = 10;
    [SerializeField] private float maxDistance = 100;

    // Offset distance from active player to camera
    private float distance = 10.0f;
    // Current X axis
    private float currentX = 0.0f;
    // Current Y axis
    private float currentY = 0.0f;

    // Properties
    /// <summary>
    /// GetCamera returns a reference to the PlayerCamera's mainCamera member variable.
    /// </summary>
    public Camera GetCamera
    {
        get
        {
            return mainCamera;
        }
    }


    void Start()
    {
        CalculateCameraAngle();

        // Clamp the pitch of the camera to min and max angles
        currentX = Mathf.Clamp(currentY, 40.0f, 70.0f);
    }

    /// <summary>
    /// Set the camera angle and position according to the active
    /// player position and min/max clamp angles.
    /// </summary>
    public void CalculateCameraAngle()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentX, currentY, 0);
        // Update the position of the camera
        transform.position = activePlayer.position + rotation * dir;
        // Point the camera to the position of the active player
        transform.LookAt(activePlayer.position);
    }

    /// <summary>
    /// UpdateCameraInput calculates changes in the camera position based on the mouse input.
    /// CH Converted Update to this method for external access 16/08/2019
    /// </summary>
    public void UpdateCameraInput()
    {
        // Use 'Mouse Y' to rotate the camera on X axis
        currentX += Input.GetAxis("Mouse Y") * camSensitivity;
        // Use 'Mouse X' to rotate the camera on Y axis
        currentY += Input.GetAxis("Mouse X") * camSensitivity;
        // Clamp the pitch of the camera to min and max angles
        currentX = Mathf.Clamp(currentX, 40.0f, 70.0f);
    }

    /// <summary>
    /// Updates the distance the camera is from the target.
    /// </summary>
    /// <param name="mouseZoom">The mouse scroll delta.</param>
    public void UpdateCameraZoomInput(float mouseZoom)
    {
        // update the zoom / distance of the camera
        distance -= mouseZoom;
        distance = Mathf.Clamp(distance, minDistance, maxDistance);
    }

    void LateUpdate()
    {
        CalculateCameraAngle();
    }


    /// <summary>
    /// SetTarget is an encapsulated method used to change the current camera target. Ensures that
    /// the new target has a Character Motor (TODO will need to change to player specific class) before
    /// assigning the new target.
    /// Added by CH 16/08/2019
    /// </summary>
    /// <param name="newTarget">A reference to new target GameObject; must have a CharacterMotor component attached.</param>
    public void SetTarget(GameObject newTarget)
    {
        if (newTarget.GetComponent<CharacterMotor>() != null || newTarget.transform.root.GetComponent<CharacterMotor>() != null)
        {
            activePlayer = newTarget.transform;
        }
        else
        {
            Debug.LogWarning("Attempted to SetTarget in the PlayerCamera that did not have a Character Motor attached.");
        }
    }
}

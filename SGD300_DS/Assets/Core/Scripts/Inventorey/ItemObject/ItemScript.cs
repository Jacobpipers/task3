﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// This is the Item UI Object logic.
/// </summary>
public class ItemScript : MonoBehaviour, IPointerClickHandler
{
    private GameObject invenPanel;
    public static GameObject selectedItem;
    public static IntVector2 selectedItemSize;
    public static bool isDragging = false;
    private float slotSize = 50;
    public ItemClass item;
    

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
        if(item.TypeName.Equals(""))
        {
            ItemClass.SetItemValues(item, item.GlobalID, 1, Random.Range(0, 3));
        }
    }

    /// <summary>
    /// Sets the Item that the item contains and it's data.
    /// </summary>
    /// <param name="passedItem"></param>
    public void SetItemObject(ItemClass passedItem)
    {
        RectTransform rect = GetComponent<RectTransform>();
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, passedItem.Size.x * slotSize);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, passedItem.Size.y * slotSize);
        item = passedItem;
        GetComponent<Image>().sprite = passedItem.Icon;
    }

    /// <summary>
    /// Select the Object and add to selected Item. On Mouse Click
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerClick(PointerEventData eventData)
    {
        SetSelectedItem(this.gameObject);
        CanvasGroup canvas = GetComponent<CanvasGroup>();
        canvas.blocksRaycasts = false;
        canvas.alpha = 0.5f;
    }

    /// <summary>
    /// 
    /// </summary>
    private void Update()
    {
        if (isDragging)
        {
            selectedItem.transform.position = Input.mousePosition;
        }

    }

    /// <summary>
    /// Add the item to the selected and set it to draging.
    /// </summary>
    /// <param name="obj"></param>
    public static void SetSelectedItem(GameObject obj)
    {
        selectedItem = obj;
        selectedItemSize = obj.GetComponent<ItemScript>().item.Size;
        isDragging = true;
        obj.transform.SetParent(GameObject.FindGameObjectWithTag("DragParent").transform);
        obj.GetComponent<RectTransform>().localScale = Vector3.one;
    }

    /// <summary>
    /// 
    /// </summary>
    public static void ResetSelectedItem()
    {
        selectedItem = null;
        selectedItemSize = IntVector2.Zero;
        isDragging = false;
    }
}

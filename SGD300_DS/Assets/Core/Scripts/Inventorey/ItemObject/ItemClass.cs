﻿using System;
using UnityEngine;

/// <summary>
/// This is a Class that holds a item data and logic 
/// Call this to create a new item
/// Created by Jacob Pipers
/// </summary>
[System.Serializable]
public class ItemClass {


    public int GlobalID;
    [HideInInspector] public int CategoryID;
    [HideInInspector] public string CategoryName;
    [HideInInspector] public int TypeID;

    [HideInInspector] public int AttackSpeed;
    [HideInInspector] public int DPS;
    [HideInInspector] public int MinDamage;
    [HideInInspector] public int MaxDamage;
    [HideInInspector] public int levelRequirment;
    [HideInInspector] public int LevelRequirmentTypeID;
    [HideInInspector] public int Value;

    public string TypeName;
    public ItemType equipmentSlot;
    [Range(1, 100)] public int Level;
    [Range(0, 3)] public int qualityInt;
    [HideInInspector] public int SizeX;
    [HideInInspector] public int SizeY;
    [HideInInspector] public IntVector2 Size;
    [HideInInspector] public Sprite Icon;
    [HideInInspector] public string SerialID;

    /// <summary>
    /// Item Quality Types
    /// </summary>
    private enum QualityEnum { Broken, Normal, Magic, Rare}
    public string GetQualityStr()
    {
        return Enum.GetName(typeof(QualityEnum), qualityInt);
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    /// <param name="ID"></param>
    /// <param name="lvl"></param>
    /// <param name="quality"></param>
    public static void SetItemValues(ItemClass item, int ID, int lvl, int quality)
    {
        item.GlobalID = ID;
        item.Level = lvl;
        item.qualityInt = quality;
        GameObject.FindGameObjectWithTag("ItemDatabase").GetComponent<LoadItemDatabase>().PassItemData(ref item);
    }
    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    public static void SetItemValues(ItemClass item)
    {
        GameObject.FindGameObjectWithTag("ItemDatabase").GetComponent<LoadItemDatabase>().PassItemData(ref item);
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="passedItem"></param>
    public ItemClass(ItemClass passedItem)//create new item by copying passedITem properties
    {
        GlobalID = passedItem.GlobalID;
        Level = passedItem.Level;
        qualityInt = passedItem.qualityInt;
    }


    /// <summary>
    /// Initial Constructor
    /// </summary>
    public ItemClass() { }


}

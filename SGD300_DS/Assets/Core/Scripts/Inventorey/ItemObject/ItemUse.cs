﻿/// <summary>
/// Item Use has static functions that allow the use of certain items.
/// Created by Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemUse 
{
    /// <summary>
    /// TODO: Maybe Delete this.
    /// Call this to use a potion. This handles how the item is used.
    /// If the item needs to be destroyed or the amount needs to be changed.
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="toHeal"></param>
    /// <returns>The Amount left after heal. If Full amount is returned nothing is used.</returns>
    public static int HealthPotionHeal(int amount, ref GameObject toHeal)
    {
        if (toHeal.GetComponent<PlayerInfo>() == null) // No Player Info Found Just Return full amount
        {
            Debug.LogWarning("No Player Info Component");
            return amount;
        }
        var curHealth = toHeal.GetComponent<Health>().GetCurrentHealth;
        var maxHealth = toHeal.GetComponent<Health>().GetMaxHealth;

        if(curHealth < maxHealth)
        {
            var difFromMax = maxHealth - curHealth; // should return at leat one.

            if (difFromMax >= amount)
            {
               // curHealth += amount;
                toHeal.GetComponent<Health>().HealSelf(amount);
                return 0;
            } else
            {
               // curHealth += difFromMax;
                toHeal.GetComponent<Health>().HealSelf(difFromMax);
                return amount - difFromMax;
            }
                
        }
        return amount;
    }


    /// <summary>
    /// Call this to use a health potion. This handles how the item is used.
    /// If the item needs to be destroyed or the amount needs to be changed.
    /// This is a non ref call.
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="toHeal"></param>
    /// <returns>The Amount left after heal. If Full amount is returned nothing is used.</returns>
    public static float HealthPotionHeal(int amount, GameObject toHeal)
    {
        if (toHeal.GetComponent<PlayerInfo>() == null) // No Player Info Found Just Return full amount
        {
            Debug.LogWarning("No Player Info Component");
            return amount;
        }
        var curHealth = toHeal.GetComponent<PlayerInfo>().GetCurrentHealth;
        var maxHealth = toHeal.GetComponent<PlayerInfo>().GetMaxHealth;

        if (curHealth < maxHealth)
        {
            var difFromMax = maxHealth - curHealth; // should return at leat one.

            if (difFromMax >= amount)
            {
                // curHealth += amount;
                toHeal.GetComponent<PlayerInfo>().HealSelf(amount);
                return 0;
            }
            else
            {
                // curHealth += difFromMax;
                toHeal.GetComponent<PlayerInfo>().HealSelf(difFromMax);
                return amount - difFromMax;
            }

        }
        return amount;
    }

    /// <summary>
    /// Call this to use a mana potion. This handles how the item is used.
    /// If the item needs to be destroyed or the amount needs to be changed.
    /// This is a non ref call.
    /// </summary>
    /// <param name="amount"></param>
    /// <param name="toHeal">The object or player to heal mana for Must Have PlayerInfo Component</param>
    /// <returns>The Amount left after heal. If Full amount is returned nothing is used.</returns>
    public static int ManaPotionHeal(int amount, GameObject toHeal)
    {
        if (toHeal.GetComponent<PlayerInfo>() == null) // No Player Info Found Just Return full amount
            return amount;
        var curMana = toHeal.GetComponent<Mana>().GetCurrentMana;
        var maxMana = toHeal.GetComponent<Mana>().GetMaxMana;

        if (curMana < maxMana)
        {
            var difFromMax = maxMana - curMana; // should return at leat one.

            if (difFromMax >= amount)
            {
                curMana += amount;
                toHeal.GetComponent<Mana>().RestoreMana(amount);
                return 0;
            }
            else
            {
                //curMana += difFromMax;
                toHeal.GetComponent<Mana>().RestoreMana(difFromMax);
                return amount - difFromMax;
            }

        }
        return amount;
    }


}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// This tooltip displays the data present on an item, enemy ect
/// Requires a data script
/// Written by Jacob
/// Written by Nathaniel Hughes
/// </summary>
[RequireComponent(typeof(ItemScript))]
public class ShowToolTip : MonoBehaviour
{

    public static ItemOverlayScript overlayScript;
    private ItemClass item;

    // Start is called before the first frame update

    /// <summary>
    /// Gets a refrence to the item component and finds the tooltip Object.
    /// </summary>
    void Start()
    {
        item = GetComponent<ItemScript>().item;
        overlayScript = GameObject.Find("ItemToolTip").GetComponent<ItemOverlayScript>();
    }


    /// <summary>
    /// Shows the tooltip when the mouse is over object.
    /// </summary>
    void OnMouseOver()
    {
        //If your mouse hovers over the GameObject with the script attached, output this message
        print("OnMouse OVer");
        DoesShowOverLay(item, true);
    }

    /// <summary>
    /// Hides the tooltip
    /// </summary>
    void OnMouseExit()
    {
        //The mouse is no longer hovering over the GameObject so output this message each frame
        DoesShowOverLay(null, false);
    }

    /// <summary>
    /// Hides the tool tip when object is destroyed as OnMouseExit is not called 
    /// </summary>
    void OnDestroy()
    {
        DoesShowOverLay(null, false);
    }

    /// <summary>
    /// A Static Function that can be called to show the tooltip
    /// </summary>
    /// <param name="item">Pass the item information</param>
    /// <param name="show">True = Show Tooltip else hide.</param>
    public static void DoesShowOverLay(ItemClass item, bool show)
    {
        if(item == null)
        {
            if(overlayScript != null)
                overlayScript.gameObject.GetComponent<CanvasGroup>().alpha = 0;
            return;
        }
        if (overlayScript != null)
        {
            /// Positions the Tooltip based on the screenposition and item position.
            var screenPosOffset = (Screen.height - overlayScript.GetComponent<RectTransform>().rect.height);
            var offset = Vector3.zero;
            if (overlayScript.gameObject.transform.position.y > screenPosOffset)
            {
                offset = new Vector3(10,10);
                overlayScript.GetComponent<RectTransform>().pivot = new Vector2(0, 1);
            } else
            {
                offset = Vector2.zero;
                overlayScript.GetComponent<RectTransform>().pivot = new Vector2(0, 0);
            }

            overlayScript.gameObject.GetComponent<CanvasGroup>().alpha = show ? 1 : 0;
            overlayScript.gameObject.transform.position = Input.mousePosition + offset;

            //   print("Item Data Show: " + item);
            overlayScript.UpdateOverlay(item);

        } else
        {
           // overlayScript = FindObjectOfType<ItemOverlayScript>().GetComponent<ItemOverlayScript>();
        }
    }


}

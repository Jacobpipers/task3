﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

/// <summary>
/// SlotSectorScript is used to help determain the area that the mouse is over in the item slot and take 
/// actions when mouse enters and leaves that slot.
/// </summary>
public class SlotSectorScript : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    public GameObject slotParent;
    public int QuadNum;
    public static IntVector2 posOffset;
    public static SlotSectorScript sectorScript;
    public static ItemOverlayScript overlayScript;
    [SerializeField]
    public InvenGridManager invenGridManager;
    private SlotScript parentSlotScript;

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
    //    invenGridManager = this.gameObject.transform.parent.GetComponent<InvenGridManager>();
        if (invenGridManager == null)
            invenGridManager = this.gameObject.GetComponentInParent<SlotScript>().invMan; // GameObject.FindGameObjectWithTag("InvenPanel").GetComponent<InvenGridManager>();

            parentSlotScript = slotParent.GetComponent<SlotScript>();

        ShowToolTip.overlayScript = overlayScript;

    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerEnter(PointerEventData eventData)
    {

        print("On Pointer Enter???");

        sectorScript = this;
        if (invenGridManager == null)
            print("No Inv Manager");
        else
        {
            invenGridManager.highlightedSlot = slotParent;


            invenGridManager.isOverUI = true;
            PosOffset();
            if (ItemScript.selectedItem != null)
            {
                invenGridManager.RefrechColor(true, invenGridManager.highlightedSlot);
            }
            if (parentSlotScript.slotInfo.storedItemObject != null && ItemScript.selectedItem == null)
            {
                if (parentSlotScript.slotInfo.gridPos.x > 1000)
                    invenGridManager.ColorChange(SlotColorHighlights.Blue, parentSlotScript.slotInfo.storedItemSize, parentSlotScript.slotInfo.storedItemStartPos);
                else
                    invenGridManager.ColorChangeLoop(SlotColorHighlights.Blue, parentSlotScript.slotInfo.storedItemSize, parentSlotScript.slotInfo.storedItemStartPos);        
            }
            if (parentSlotScript.slotInfo.storedItemObject != null && invenGridManager.GetComponent<CanvasGroup>().interactable)
            {
                ShowToolTip.DoesShowOverLay(parentSlotScript.slotInfo.storedItemClass, true);
            }
        }

    }

    /// <summary>
    /// 
    /// </summary>
    public void PosOffset()
    {
        if (ItemScript.selectedItemSize.x != 0 && ItemScript.selectedItemSize.x % 2 == 0)
        {
            switch (QuadNum)
            {
                case 1:
                    posOffset.x = 0; break;
                case 2:
                    posOffset.x = -1; break;
                case 3:
                    posOffset.x = 0; break;
                case 4:
                    posOffset.x = -1; break;
                default: break;
            }
        }
        if (ItemScript.selectedItemSize.y != 0 && ItemScript.selectedItemSize.y % 2 == 0)
        {
            switch (QuadNum)
            {
                case 1:
                    posOffset.y = -1; break;
                case 2:
                    posOffset.y = -1; break;
                case 3:
                    posOffset.y = 0; break;
                case 4:
                    posOffset.y = 0; break;
                default: break;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="eventData"></param>
    public void OnPointerExit(PointerEventData eventData)
    {
        if (invenGridManager == null)
            print("No INV MAN");
        else
        {
            sectorScript = null;
            invenGridManager.isOverUI = false;
            invenGridManager.highlightedSlot = null;
            //overlayScript.UpdateOverlay(null);
            //overlayScript.gameObject.GetComponent<CanvasGroup>().alpha = 0;
            ShowToolTip.DoesShowOverLay(null, false);

            if (ItemScript.selectedItem != null)
            {
                invenGridManager.RefrechColor(false, invenGridManager.highlightedSlot);
            }
            posOffset = IntVector2.Zero;
            if (parentSlotScript.slotInfo.storedItemObject != null && ItemScript.selectedItem == null)
            {
                if (parentSlotScript.slotInfo.gridPos.x > 1000)
                    invenGridManager.ColorChange(SlotColorHighlights.Blue2, parentSlotScript.slotInfo.storedItemSize, parentSlotScript.slotInfo.storedItemStartPos);
                else
                    invenGridManager.ColorChangeLoop(SlotColorHighlights.Blue2, parentSlotScript.slotInfo.storedItemSize, parentSlotScript.slotInfo.storedItemStartPos);
            }
        }
    }
        
}

﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// InvenGridManager handles most of the inventorey logic
/// This includes the logic in adding - Swaping and moving items in the inventorey.
/// Created By Jacob Pipers
/// </summary>
public class InvenGridManager : MonoBehaviour {

    public GameObject[,] slotGrid;
    public GameObject highlightedSlot;
   
    public GameObject worldItemPrefab;
    public Transform dropParent;
    public GameObject playerController;
    public GameObject InventoreyOwner;
    public bool isOverUI;

    [HideInInspector]
    public IntVector2 gridSize;

    public GameObject selectedButton;
    public ObjectPoolScript itemEquipPool;

    protected IntVector2 totalOffset, checkSize, checkStartPos;
    protected IntVector2 otherItemPos, otherItemSize; //*3

    protected int checkState;
    protected bool isOverEdge = false;

    public ItemOverlayScript overlayScript;


    /// <summary>
    /// 
    /// </summary>
    protected virtual void Start()
    {
        CloseView();
    }


    /// <summary>
    /// Add the Item to the equipment slot and update the stats.
    /// </summary>
    /// <param name="item"></param>
    public void AddItemToEquipmentSlot(GameObject item)
    {
        SlotScript instanceScript;

        print("item added to equipment: " + item.name);

        //set each slot parameters
        instanceScript = highlightedSlot.GetComponent<SlotScript>();
        instanceScript.slotInfo.storedItemObject = item;
        instanceScript.slotInfo.storedItemClass = item.GetComponent<ItemScript>().item;
        instanceScript.slotInfo.storedItemStartPos = totalOffset;
        instanceScript.slotInfo.isOccupied = true;
        highlightedSlot.GetComponent<Image>().color = SlotColorHighlights.Gray;
           
        item.transform.SetParent(dropParent);
        item.GetComponent<RectTransform>().pivot = Vector2.zero;
        item.transform.position = highlightedSlot.transform.position;

        

        item.GetComponent<CanvasGroup>().alpha = 1f;
        if (highlightedSlot != null)
            overlayScript.UpdateOverlay(highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemClass);

        var updateInfo = FindObjectsOfType<PlayerInfo>();
        foreach (var info in updateInfo)
        {
            info.LoadStats();
        }

    }


    /// <summary>
    /// Add a item to the inventorey in the next availbele spot passing in the itemClass
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    public bool AddItemToInv(ItemClass item)
    {
        return AddItemToInv(item.GlobalID, item);
    }

    /// <summary>
    /// AddItemToInv adds an item to the inventorey if there is sufficient room. 
    /// </summary>
    /// <param name="itemIndex"> The Item to load by index number</param>
    /// TODO: Add Level and Quality to params.
    /// TODO: Expand to better allow multiple characters to be able to have own inventory
    /// <returns>Returns true or false if item can be added to Inventorey.</returns>
    public bool AddItemToInv(int itemIndex = -1, ItemClass itemProperties = null, GameObject InvToAddTo = null)
    {
        if(InvToAddTo == null)
        {
            InvToAddTo = gameObject;
        }

        if (itemIndex == -1  && itemProperties == null)
            return false;

        ItemClass item = new ItemClass();
        ItemClass newItem = new ItemClass(item);
        if (itemProperties == null)
            ItemClass.SetItemValues(newItem, itemIndex, 1, Random.Range(0, 3));
        else
            ItemClass.SetItemValues(newItem, itemProperties.GlobalID, itemProperties.Level, itemProperties.qualityInt);
        

        GameObject itemObject = itemEquipPool.GetObject();
        itemObject.GetComponent<ItemScript>().SetItemObject(newItem);
        IntVector2 itemSize = itemObject.GetComponent<ItemScript>().item.Size;

        SlotScript[] slotScripts = GetComponentsInChildren<SlotScript>();
        // Loops through each slot and checks if it there is enough space to fit the item.
        foreach(SlotScript script in slotScripts)
        {
            
            if (itemObject != null && !isOverEdge)
            {
                SlotScript instanceScript;
                IntVector2 itemSizeL = itemObject.GetComponent<ItemScript>().item.Size;

                if (script.slotInfo.isOccupied)
                {
                    continue;
                }

                for (int y = 0; y < itemSizeL.y; y++)
                {
                    for (int x = 0; x < itemSizeL.x; x++)
                    {

                        //set each slot parameters
                        instanceScript = slotGrid[script.slotInfo.gridPos.x + x, script.slotInfo.gridPos.y + y].GetComponent<SlotScript>();
                        instanceScript.slotInfo.storedItemObject = itemObject;
                        instanceScript.slotInfo.storedItemClass = itemObject.GetComponent<ItemScript>().item;
                        instanceScript.slotInfo.storedItemSize = itemSizeL;
                        instanceScript.slotInfo.storedItemStartPos = script.slotInfo.gridPos;
                        instanceScript.slotInfo.isOccupied = true;
                       // playerController.GetComponent<PlayerInput>().TargetPlayerCharacter.GetComponent<CharactersInventoreyInfo>().AddItemToCharacterSlot(instanceScript.slotInfo);
                       
                        slotGrid[script.slotInfo.gridPos.x + x, script.slotInfo.gridPos.y + y].GetComponent<Image>().color = SlotColorHighlights.Gray;
                    }
                }
                //set dropped parameters
                itemObject.transform.SetParent(dropParent);
                itemObject.GetComponent<RectTransform>().pivot = Vector2.zero;
                itemObject.transform.position = slotGrid[script.slotInfo.gridPos.x, script.slotInfo.gridPos.y].transform.position;

            //    var inv  = InvToAddTo.GetComponent<CharactersInventoreyInfo>();
            //    inv.items.Add(itemObject.GetComponent<ItemScript>().item);

                var canvasGroup = transform.GetComponent<CanvasGroup>();
                if (canvasGroup.alpha == 1f)
                    itemObject.GetComponent<CanvasGroup>().alpha = 1f;
                else
                    itemObject.GetComponent<CanvasGroup>().alpha = 0f;

                return true;
                        
            }

        }
        return false;
    }

    /// <summary>
    /// Will use to update the Active inventorey.
    /// </summary>
    public void UpdateUI()
    {
        var tempGrid =  playerController.GetComponent<CharactersInventoreyInfo>().playersInvGrid;
        SlotScript[] slotScripts = GetComponentsInChildren<SlotScript>();
        foreach (SlotScript slot in slotScripts)
        {
            slot.slotInfo = tempGrid[slot.slotInfo.gridPos.x, slot.slotInfo.gridPos.y];
            //set dropped parameters
        }
    }

    protected virtual void Update()
    {
        /// On left Click on Inventorey
        if (Input.GetMouseButtonUp(0) && GetComponent<CanvasGroup>().interactable)
        {
            print("Mouse Pressed");
            // Check If Equipment Slot 
            if (highlightedSlot != null && ItemScript.selectedItem != null && isOverEdge)
            {
                EquipItemLogic();
            }
            /// Checks if is in Inventorey and that the slot is empty
            else if (highlightedSlot != null && ItemScript.selectedItem != null && !isOverEdge)
            {
                // Check state of slot
                switch (checkState)
                {
                    case 0: //store on empty slots
                        StoreItem(ItemScript.selectedItem, false);
                        ColorChangeLoop(SlotColorHighlights.Blue, ItemScript.selectedItemSize, totalOffset);
                        ItemScript.ResetSelectedItem();
                        RemoveSelectedButton();
                        break;
                    case 1: //swap items
                        ItemScript.SetSelectedItem(SwapItem(ItemScript.selectedItem));
                        SlotSectorScript.sectorScript.PosOffset();
                        ColorChangeLoop(SlotColorHighlights.Gray, otherItemSize, otherItemPos); //*1
                        RefrechColor(true, highlightedSlot);
                        RemoveSelectedButton();
                        break;
                }
            } else if (ItemScript.selectedItem != null)
            {
            }
            // retrieve items
            else if (highlightedSlot != null && ItemScript.selectedItem == null && highlightedSlot.GetComponent<SlotScript>().slotInfo.isOccupied == true)
            {
                RetrieveItemInSlot();

            }
        // Right Click Logic
        } else if(Input.GetMouseButtonUp(1) && GetComponent<CanvasGroup>().interactable)
        {
            print("Drop Item");
            if (highlightedSlot != null)
            {
                var curItem = highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemClass;
                if (curItem == null)
                    return;
                if ((ItemType)curItem.CategoryID == ItemType.Other)
                {
                    switch (curItem.TypeID)
                    {
                        case 0:
                            print("Potion Used?");
                            var potionLeft = ItemUse.HealthPotionHeal(curItem.DPS, InventoreyOwner);
                            if (potionLeft <= 0)
                            {
                                // Destroy Potion
                                ItemScript.SetSelectedItem(GetItem(highlightedSlot));
                                SlotSectorScript.sectorScript.PosOffset();
                                RefrechColor(true, highlightedSlot);
                                RemoveItem();
                            } else
                            {
                                curItem.DPS = (int)potionLeft;
                            }
                            break;
                        case 1:
                            var manaPotionLeft = ItemUse.ManaPotionHeal(curItem.DPS, InventoreyOwner);
                            if (manaPotionLeft <= 0)
                            {
                                // Destroy Potion
                                ItemScript.SetSelectedItem(GetItem(highlightedSlot));
                                SlotSectorScript.sectorScript.PosOffset();
                                RefrechColor(true, highlightedSlot);
                                RemoveItem();
                            }
                            else
                            {
                                curItem.DPS = manaPotionLeft;
                            }
                            break;
                    }

                } else
                {
                    ItemScript.SetSelectedItem(GetItem(highlightedSlot));
                    SlotSectorScript.sectorScript.PosOffset();
                    RefrechColor(true, highlightedSlot);
                    DropItem();
                }
            }
            if (ItemScript.selectedItem != null)
            {
                 DropItem();
            }
        }
    }

    /// <summary>
    /// Gets the Current Item In the slot and adds to selected item.
    /// </summary>
    private void RetrieveItemInSlot()
    {
        if (highlightedSlot.GetComponent<SlotScript>().slotInfo.gridPos.x > 1000)
            ColorChange(SlotColorHighlights.Gray, highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemSize, highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemStartPos);

        else
            ColorChangeLoop(SlotColorHighlights.Gray, highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemSize, highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemStartPos);

        ItemScript.SetSelectedItem(GetItem(highlightedSlot));
        SlotSectorScript.sectorScript.PosOffset();
        RefrechColor(true, highlightedSlot);
        // Get all the current players and update the Info.
        var updateInfo = FindObjectsOfType<PlayerInfo>();
        // When Item is removed Update each characters UI Stats.
        foreach (var info in updateInfo)
        {
            info.LoadStats();
        }
    }

    /// <summary>
    /// The Logic Used to Equip Items to the Character.
    /// </summary>
    private void EquipItemLogic()
    {
        var tempCurSlot = highlightedSlot.GetComponent<SlotScript>();
        if (tempCurSlot.slotInfo.gridPos.x > 1000)
        {
            // Check if Can Go into that slot.
            if (tempCurSlot.slotInfo.allowedItems == ItemScript.selectedItem.GetComponent<ItemScript>().item.equipmentSlot)
            {
                switch (checkState)
                {
                    case 0: //store on empty slots
                        AddItemToEquipmentSlot(ItemScript.selectedItem);
                        ColorChange(SlotColorHighlights.Blue, ItemScript.selectedItemSize, totalOffset);
                        ItemScript.ResetSelectedItem();
                        RemoveSelectedButton();
                        break;
                    case 1: //swap items
                        ItemScript.SetSelectedItem(SwapItem(ItemScript.selectedItem));
                        SlotSectorScript.sectorScript.PosOffset();
                        ColorChange(SlotColorHighlights.Gray, otherItemSize, otherItemPos); //*1
                        RefrechColor(true, highlightedSlot);
                        RemoveSelectedButton();
                        break;
                }
            }
            else
            {
                print("ERROR: This Item can not go there");
            }

        }
    }

    /// <summary>
    /// Call this to use Potions.
    /// </summary>
    /// <param name="curItem"></param>
    /// <param name="curSlot"></param>
    public void PotionHeal(ItemClass curItem, GameObject curSlot)
    {
        if ((ItemType)curItem.CategoryID == ItemType.Other)
        {
            switch (curItem.TypeID)
            {
                case 0:
                    print("Potion Used?");
                    var potionLeft = ItemUse.HealthPotionHeal(curItem.DPS, InventoreyOwner);
                    if (potionLeft <= 0)
                    {
                        // Destroy Potion
                        ItemScript.SetSelectedItem(GetItem(curSlot));
                      //  SlotSectorScript.sectorScript.PosOffset();
                        RefrechColor(true, curSlot);
                        RemoveItem();
                    }
                    else
                    {
                        curItem.DPS = (int)potionLeft;
                    }
                    break;
                case 1:
                    var manaPotionLeft = ItemUse.ManaPotionHeal(curItem.DPS, InventoreyOwner);
                    if (manaPotionLeft <= 0)
                    {
                        // Destroy Potion
                        ItemScript.SetSelectedItem(GetItem(curSlot));
                      //  SlotSectorScript.sectorScript.PosOffset();
                        RefrechColor(true, curSlot);
                        RemoveItem();
                    }
                    else
                    {
                        curItem.DPS = manaPotionLeft;
                    }
                    break;
            }

        }
    }

    public virtual void DropItem()
    {
        if (playerController == null)
            playerController = GameObject.FindGameObjectWithTag("PlayerController");

        var curPlayer = playerController.GetComponent<PlayerController>().player;

        // Drop Item - Need to add all the properties.
        var spawnpos = curPlayer.transform.position;

        spawnpos += new Vector3(Random.Range(-2, 2), 0.5f, 0);
   

        SpawnItem(ItemScript.selectedItem.GetComponent<ItemScript>().item, spawnpos, worldItemPrefab, overlayScript);

        Destroy(ItemScript.selectedItem);
        ItemScript.ResetSelectedItem();
        RemoveSelectedButton();
        ItemScript.selectedItem = null;
    }

    /// <summary>
    /// 
    /// </summary>
    public virtual void RemoveItem()
    {
        if (playerController == null)
            playerController = GameObject.FindGameObjectWithTag("PlayerController");

        var curPlayer = playerController.GetComponent<PlayerController>().player;

        // Drop Item - Need to add all the properties.
        var spawnpos = curPlayer.transform.position;

        Destroy(ItemScript.selectedItem);
        ItemScript.ResetSelectedItem();
        RemoveSelectedButton();
        ItemScript.selectedItem = null;
    }

    public virtual void RemoveItem(GameObject itemToRemove)
    {
        if (playerController == null)
            playerController = GameObject.FindGameObjectWithTag("PlayerController");

        var curPlayer = InventoreyOwner;

        // Drop Item - Need to add all the properties.
        var spawnpos = curPlayer.transform.position;

        Destroy(itemToRemove);
        ItemScript.ResetSelectedItem();
        RemoveSelectedButton();
        ItemScript.selectedItem = null;
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    /// <param name="spawnPos"></param>
    /// <param name="itemPrefab"></param>
    /// <param name="toolTipObject"></param>
    public static void SpawnItem(ItemClass item, Vector3 spawnPos, GameObject itemPrefab, ItemOverlayScript toolTipObject)
    {
        GameObject temp = Instantiate(itemPrefab, spawnPos, Quaternion.identity);
        ShowToolTip.overlayScript = toolTipObject;
//        temp.GetComponent<Rigidbody2D>().gravityScale = 0;
        temp.GetComponent<ItemScript>().item = item;
    }

    /// <summary>
    /// Checks the area of the item and if it is within the Inventorey.
    /// </summary>
    /// <param name="itemSize"></param>
    private void CheckArea(IntVector2 itemSize, GameObject curSlot) //*2
    {
        IntVector2 halfOffset;
        IntVector2 overCheck;
        halfOffset.x = (itemSize.x - (itemSize.x % 2 == 0 ? 0 : 1)) / 2;
        halfOffset.y = (itemSize.y - (itemSize.y % 2 == 0 ? 0 : 1)) / 2;
        totalOffset = curSlot.GetComponent<SlotScript>().slotInfo.gridPos - (halfOffset + SlotSectorScript.posOffset);
        checkStartPos = totalOffset;
        checkSize = itemSize;
        overCheck = totalOffset + itemSize;
        isOverEdge = false;
        //checks if item to stores is outside grid

        // Is Equipment Slot
        
        if (overCheck.x > gridSize.x)
        {
            checkSize.x = gridSize.x - totalOffset.x;
            isOverEdge = true;
        }
        if (totalOffset.x < 0)
        {
            checkSize.x = itemSize.x + totalOffset.x;
            checkStartPos.x = 0;
            isOverEdge = true;
        }
        if (overCheck.y > gridSize.y)
        {
            checkSize.y = gridSize.y - totalOffset.y;
            isOverEdge = true;
        }
        if (totalOffset.y < 0)
        {
            checkSize.y = itemSize.y + totalOffset.y;
            checkStartPos.y = 0;
            isOverEdge = true;
        }
    }

    /// <summary>
    /// Used to check that the slots the Item area (while item selected) is covering any other items or in the grid
    /// </summary>
    /// <param name="itemSize"></param>
    /// <returns>0: area is not occupied 1: one slot item is occupied 2: 1+ slot is occupied</returns>
    private int SlotCheck(IntVector2 itemSize, GameObject curSlot)//*2
    {
        GameObject obj = null;
        SlotScript instanceScript;

        if (curSlot.GetComponent<SlotScript>().slotInfo.gridPos.x > 1000)
        {
            instanceScript = highlightedSlot.GetComponent<SlotScript>();

            if(instanceScript.slotInfo.isOccupied)
            {
                return 1;
            } else
            {
                return 0;
            }

        }


        if (!isOverEdge)
        {
            for (int y = 0; y < itemSize.y; y++)
            {
                for (int x = 0; x < itemSize.x; x++)
                {
                    instanceScript = slotGrid[checkStartPos.x + x, checkStartPos.y + y].GetComponent<SlotScript>();
                    if (instanceScript.slotInfo.isOccupied)
                    {
                        if (obj == null)
                        {
                            obj = instanceScript.slotInfo.storedItemObject;
                            otherItemPos = instanceScript.slotInfo.storedItemStartPos;
                            otherItemSize = obj.GetComponent<ItemScript>().item.Size;
                        }
                        else if (obj != instanceScript.slotInfo.storedItemObject)
                            return 2; // if cheack Area has 1+ item occupied
                    }
                }
            }
            if (obj == null)
                return 0; // if checkArea is not occupied
            else
                return 1; // if checkArea only has 1 item occupied
        }
        return 2; // check areaArea is over the grid
    }

    /// <summary>
    /// Used To Change the Colors as the item is moved around to give user feedbac
    /// </summary>
    /// <param name="enter"></param>
    public void RefrechColor(bool enter, GameObject curSlot)
    {
        if (enter)
        {
            CheckArea(ItemScript.selectedItemSize, curSlot);
            checkState = SlotCheck(checkSize, curSlot);
            switch (checkState)
            {
                case 0: ColorChangeLoop(SlotColorHighlights.Green, checkSize, checkStartPos); break; //no item in area
                case 1:
                    ColorChangeLoop(SlotColorHighlights.Yellow, otherItemSize, otherItemPos); //1 item on area and can swap
                    ColorChangeLoop(SlotColorHighlights.Green, checkSize, checkStartPos);
                    break;
                case 2: ColorChangeLoop(SlotColorHighlights.Red, checkSize, checkStartPos); break; //invalid position (more then 2 items in area or area is outside grid)
            }
        }
        else //on pointer exit. resets colors
        {
            isOverEdge = false;
            //checkArea(); //commented out for performance. may cause bugs if not included
            
            ColorChangeLoop2(checkSize, checkStartPos);
            if (checkState == 1)
            {
                ColorChangeLoop(SlotColorHighlights.Blue2, otherItemSize, otherItemPos);
            }
        }
    }
    
    /// <summary>
    /// 
    /// </summary>
    /// <param name="color"></param>
    /// <param name="size"></param>
    /// <param name="startPos"></param>
    public void ColorChangeLoop(Color32 color, IntVector2 size, IntVector2 startPos)
    {
        for (int y = 0; y < size.y; y++)
        {
            for (int x = 0; x < size.x; x++)
            {
                slotGrid[startPos.x + x, startPos.y + y].GetComponent<Image>().color = color;
            }
        }
    }

    public void ColorChange(Color32 color, IntVector2 size, IntVector2 startPos)
    {
        if(highlightedSlot != null)
            highlightedSlot.GetComponent<Image>().color = color;
    }


    public void ColorChange2(Color32 color, IntVector2 size, IntVector2 startPos)
    {
        GameObject slot;
       
        slot = highlightedSlot;
        if (slot.GetComponent<SlotScript>().slotInfo.isOccupied != false)
        {
            highlightedSlot.GetComponent<Image>().color = SlotColorHighlights.Blue2;
        }
        else
        {
            highlightedSlot.GetComponent<Image>().color = SlotColorHighlights.Gray;
        }
    }


    /// <summary>
    /// 
    /// </summary>
    /// <param name="size"></param>
    /// <param name="startPos"></param>
    public void ColorChangeLoop2(IntVector2 size, IntVector2 startPos)
    {
        GameObject slot;
        for (int y = 0; y < size.y; y++)
        {
            for (int x = 0; x < size.x; x++)
            {
                slot = slotGrid[startPos.x + x, startPos.y + y];
                if (slot.GetComponent<SlotScript>().slotInfo.isOccupied != false)
                {
                    slotGrid[startPos.x + x, startPos.y + y].GetComponent<Image>().color = SlotColorHighlights.Blue2;
                }
                else
                {
                    slotGrid[startPos.x + x, startPos.y + y].GetComponent<Image>().color = SlotColorHighlights.Gray;
                }
            }
        }
    }

    /// <summary>
    /// StoreItem Loops through each slot to apply the new item attributes to that slot.
    /// </summary>
    /// <param name="item"></param>
    /// <param name="higlight"></param>
    protected void StoreItem(GameObject item, bool higlight = true)
    {
        SlotScript instanceScript;
        IntVector2 itemSizeL = item.GetComponent<ItemScript>().item.Size;
        for (int y = 0; y < itemSizeL.y; y++)
        {
            for (int x = 0; x < itemSizeL.x; x++)
            {
                int tempx = totalOffset.x + x; int tempY = totalOffset.y + y;
                //set each slot parameters
                print("Slot Grid " + tempx + "," + tempY);
                instanceScript = slotGrid[totalOffset.x + x, totalOffset.y + y].GetComponent<SlotScript>();
                instanceScript.slotInfo.storedItemObject = item;
                instanceScript.slotInfo.storedItemClass = item.GetComponent<ItemScript>().item;
                instanceScript.slotInfo.storedItemSize = itemSizeL;
                instanceScript.slotInfo.storedItemStartPos = totalOffset;
                instanceScript.slotInfo.isOccupied = true;
                slotGrid[totalOffset.x + x, totalOffset.y + y].GetComponent<Image>().color = SlotColorHighlights.Gray;
            }
        }//set dropped parameters
        item.transform.SetParent(dropParent);
        item.GetComponent<RectTransform>().pivot = Vector2.zero;
        item.transform.position = slotGrid[totalOffset.x, totalOffset.y].transform.position;

        print("Debug: Item: " + item.name);

        item.GetComponent<CanvasGroup>().alpha = 1f;
        if(highlightedSlot != null && higlight)
            overlayScript.UpdateOverlay(highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemClass);
    }


    public virtual void OpenView()
    {
        foreach (CanvasGroup tempDrop in dropParent.GetComponentsInChildren<CanvasGroup>())
        {
            tempDrop.alpha = 1f;
            tempDrop.interactable = true;
        }
        var canvasGroup = transform.GetComponent<CanvasGroup>();
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 1f;
    }

    public virtual void CloseView()
    {
        foreach (CanvasGroup tempDrop in dropParent.GetComponentsInChildren<CanvasGroup>())
        {
                tempDrop.alpha = 0f;
                tempDrop.interactable = false;
        }
        var canvasGroup = transform.GetComponent<CanvasGroup>();
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.alpha = 0f;

    }


    public void UpdateView()
    {
    }

    /// <summary>
    /// GetItem is used to get the details of the item in the current slot that is selected.
    /// </summary>
    /// <param name="slotObject"></param>
    /// <returns></returns>
    protected GameObject GetItem(GameObject slotObject)
    {
        SlotScript slotObjectScript = slotObject.GetComponent<SlotScript>();
        GameObject retItem = slotObjectScript.slotInfo.storedItemObject;
        IntVector2 tempItemPos = slotObjectScript.slotInfo.storedItemStartPos;
        IntVector2 itemSizeL = retItem.GetComponent<ItemScript>().item.Size;

        SlotScript instanceScript;
        for (int y = 0; y < itemSizeL.y; y++)
        {
            for (int x = 0; x < itemSizeL.x; x++)
            {
                //reset each slot parameters
                if (slotObject.GetComponent<SlotScript>().slotInfo.gridPos.x > 1000)
                    instanceScript = highlightedSlot.GetComponent<SlotScript>();
                else
                    instanceScript = slotGrid[tempItemPos.x + x, tempItemPos.y + y].GetComponent<SlotScript>();

                instanceScript.slotInfo.storedItemObject = null;
                instanceScript.slotInfo.storedItemSize = IntVector2.Zero;
                instanceScript.slotInfo.storedItemStartPos = IntVector2.Zero;
                instanceScript.slotInfo.storedItemClass = null;
                instanceScript.slotInfo.isOccupied = false;
            }
        }//returned item set item parameters
        retItem.GetComponent<RectTransform>().pivot = new Vector2(0.5f, 0.5f);
        retItem.GetComponent<CanvasGroup>().alpha = 0.5f;
        retItem.transform.position = Input.mousePosition;
        overlayScript.UpdateOverlay(null);
        return retItem;
    }

    /// <summary>
    /// Swap Item Used to store temp item as other item is writen to slots
    /// </summary>
    /// <param name="item"></param>
    /// <returns></returns>
    private GameObject SwapItem(GameObject item)
    {
        GameObject tempItem;
        if (highlightedSlot.GetComponent<SlotScript>().slotInfo.gridPos.x > 1000)
            tempItem = GetItem(highlightedSlot);
        else
            tempItem = GetItem(slotGrid[otherItemPos.x, otherItemPos.y]);


        if (highlightedSlot.GetComponent<SlotScript>().slotInfo.gridPos.x > 1000)
            AddItemToEquipmentSlot(item);
        else
            StoreItem(item);
        return tempItem;
    }

    /// <summary>
    /// Removes the Selected Button
    /// </summary>
    public void RemoveSelectedButton()
    {
        if (selectedButton != null)
        {
            selectedButton = null;

        }
    }
    
}

/// <summary>
/// 
/// </summary>
public struct SlotColorHighlights
{
    public static Color32 Green
    { get { return new Color32(127, 223, 127, 255); } }
    public static Color32 Yellow
    { get { return new Color32(223, 223, 63, 255); } }
    public static Color32 Red
    { get { return new Color32(223, 127, 127, 255); } }
    public static Color32 Blue
    { get { return new Color32(159, 159, 223, 255); } }
    public static Color32 Blue2
    { get { return new Color32(191, 191, 223, 255); } }
    public static Color32 Gray
    { get { return new Color32(223, 223, 223, 255); } }
}

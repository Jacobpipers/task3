﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Slot Script gets added to each slot that is created.
/// This contains information about the slot such as if it is Occupied
/// text is for debugging the slot position.
/// Created by Jacob Pipers
/// </summary>
public class SlotScript : MonoBehaviour
{

    public InvenGridManager invMan;
    public SlotClass slotInfo;

    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
        //   text.text = gridPos.x + "," + gridPos.y;

        var slotsQuads = GetComponentsInChildren<SlotSectorScript>();
        foreach(var slotQuad in slotsQuads)
        {
            slotQuad.invenGridManager = invMan;
        }

    }
}

/// <summary>
/// TODO: Move to ItemClass or other enum script.
/// This Holds all the Item Types Used For Slot Type.
/// </summary>
[System.Serializable]
public enum ItemType
{
    Weapon,
    RangedWeapon,
    Helmet,
    SpellBook,
    Gloves,
    Amulet,
    BodyArmour,
    Rings,
    Sheild,
    Boots,
    Other
}

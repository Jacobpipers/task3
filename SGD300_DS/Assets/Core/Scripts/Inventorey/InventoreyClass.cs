﻿/// <summary>
/// This holds the structure of the inventorey slot class such as 
/// the Item that is stored the allowed items and if the spot is occupied. etc...
/// Created by Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct SlotClass
{

    public IntVector2 gridPos;

    public GameObject storedItemObject;
    public IntVector2 storedItemSize;
    public IntVector2 storedItemStartPos;
    public ItemClass storedItemClass;
    public ItemType allowedItems;
    public bool isOccupied;


}

﻿/// <summary>
/// Add to Npcs with a shop and poppulate with the items tha the shop has.
/// Created By Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItems : MonoBehaviour , IInteractable
{

    /// <summary>
    /// A List of items that this shop has.
    /// </summary>
    [SerializeField] public ItemClass[] shopItems;



    [SerializeField] private GameObject shopWindowPrefab;
    [SerializeField] private GameObject parent;
    
    /// <summary>
    /// The UI Refrence
    /// </summary>
    private ShopManager shopPanel;


    void Start()
    {
      //  OpenShop();
    }

    public void LoadItems()
    {
        foreach(ItemClass item in shopItems)
        {
            shopPanel.AddItemToInv(item);
        }
    }

    GameObject temp;
    /// <summary>
    /// Spawns the Shop UI.
    /// </summary>
    public void OpenShop()
    {
        if(temp == null)
        {
            temp = Instantiate(shopWindowPrefab);
            temp.transform.SetParent(parent.transform);
            temp.GetComponent<RectTransform>().anchoredPosition = new Vector2(-50, -50);
            shopPanel = temp.GetComponentInChildren<ShopManager>();
            shopPanel.itemEquipPool = FindObjectOfType<ObjectPoolScript>();
            shopPanel.overlayScript = FindObjectOfType<ItemOverlayScript>();
            shopPanel.playerController = FindObjectOfType<PlayerController>().gameObject;
            shopPanel.closeButton.onClick.AddListener(CloseShop);
           LoadItems();
            //
        }
    }

    /// <summary>
    /// Called to Close the shop.
    /// </summary>
    public void CloseShop()
    {
        Destroy(temp);
    }

    /// <summary>
    /// This is just to test the item shop on interact and can be removed if shop is via button
    /// </summary>
    public void Interact()
    {
        OpenShop();
    }
}

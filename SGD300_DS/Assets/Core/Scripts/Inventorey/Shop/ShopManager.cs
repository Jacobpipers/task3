﻿/// <summary>
/// This Manages a shope UI and Logic 
/// This inherits from the Inventerory Manager.
/// 
/// Created by Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopManager : InvenGridManager
{

    [HideInInspector] public ItemClass[] shopItems;
    public Button closeButton;

    // Start is called before the first frame update
    void Awake()
    {
      //  dropParent = new GameObject("DropParent", typeof(RectTransform)).transform;
      //  dropParent.SetParent(GameObject.Find("MiscParent").transform);
    }

    /// <summary>
    /// Overides the Start.
    /// </summary>
    protected override void Start()
    {

    }

    /// <summary>
    /// Adds Shop Only logic to the RemoveItem Logic
    /// </summary>
    public override void RemoveItem()
    {
        base.RemoveItem();

        ColorChangeLoop(SlotColorHighlights.Gray, highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemSize, highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemStartPos);

       
        RefrechColor(true, highlightedSlot);

        Destroy(GetItem(highlightedSlot));
      
    }

    /// <summary>
    /// overides the CloseView Behaviour
    /// </summary>
    public override void CloseView()
    {
        //base.CloseView();
    }

    /// <summary>
    /// Overides the Update Loop and allows the item to be bought by the first party member.
    /// </summary>
    protected override void Update()
    {
      //  base.Update();

        if(Input.GetMouseButtonUp(0))
        {


            if (highlightedSlot != null && ItemScript.selectedItem != null)
            {
                switch (checkState)
                {
                    case 0: 
                        var goldRef = playerController.GetComponent<GlobalStats>();
                        goldRef.IncreaseGold(ItemScript.selectedItem.GetComponent<ItemScript>().item.Value);
                        goldRef.UpdateGoldUI();

                        StoreItem(ItemScript.selectedItem, false);
                        ColorChangeLoop(SlotColorHighlights.Blue, ItemScript.selectedItemSize, totalOffset);
                        ItemScript.ResetSelectedItem();
                        RemoveSelectedButton();
                        break;
                    case 1: //swap items
                       
                        break;
                }
            }


            if (highlightedSlot != null && ItemScript.selectedItem == null)
            {
                print("Buy Item");
                // Check Gold 
                if(playerController.GetComponent<GlobalStats>().Gold >= highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemClass.Value)
                {
                    print("Gold: " + playerController.GetComponent<GlobalStats>().Gold + " Cost: " + highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemClass.Value);
                    // If Enough Add To Selected Player Inventorey This will need to be changed to cur selected character
                    if (playerController.GetComponent<PlayerController>().PartyMembers[0].GetComponent<CharactersInventoreyInfo>().invRef.AddItemToInv(highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemClass))
                    {
                        // Remove From Shop and Reduce Gold
                        var goldRef = playerController.GetComponent<GlobalStats>();
                        goldRef.Gold -= highlightedSlot.GetComponent<SlotScript>().slotInfo.storedItemClass.Value;
                        goldRef.UpdateGoldUI();
                        RemoveItem();
                    } else
                    {
                        print("Not Enough Space");
                    }
                } else
                {
                    print("Not Enough Gold");
                }
                



            }

        }


    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// This will handle saving and loading items that the player has in inventorey
/// This should be self contained as much as possible allowing it to be called from any point of the game.
/// </summary>
public class LoadSaveItems : MonoBehaviour {

    public LoadItemDatabase itemDB;
 //   public ItemListManager listManager;

    public TextAsset startItemsFile;
    public TextAsset presetItemsFile;
    public TextAsset saveFile;

    private List<ItemClass> startItemList = new List<ItemClass>();

    private void Start()
    {
        startItemList = LoadItems(startItemsFile); // Load Items into the inventorey. 
    }

    // The Data that will need to be loaded. (Need to work out what needs to be saved and loaded).
    public List<ItemClass> LoadItems(TextAsset itemFile)
    {
        string[][] grid = CsvReadWrite.LoadTextFile(itemFile);
        List<ItemClass> itemList = new List<ItemClass>();
        for (int i = 1; i < grid.Length; i++)
        {
            ItemClass item = new ItemClass();
            ItemClass.SetItemValues(item, Int32.Parse(grid[i][0]), Int32.Parse(grid[i][1]), Int32.Parse(grid[i][2]));
            itemList.Add(item);
        }
        return itemList;
    }

    public void SaveItemsToFile(List<ItemClass> itemList, TextAsset file)
    {

    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;


/// <summary>
/// The LoadItemDatabase Is Used to load the Database File the is a test item file that was created to aid in testing the inventorey system
/// This file can be expanded on to have the item attributes as needed and can be added to the ItemData Class.
/// Created by Jacob Pipers
/// </summary>
public class LoadItemDatabase : MonoBehaviour {

    public TextAsset dbFile;

    public List<string> TypeNameList = new List<string>();


    private void Awake()
    {
        LoadDb(dbFile);
    }

    public class ItemData
    {
        public int GlobalID;
        public int CategoryID;
        public string CategoryName;
        public int TypeID;
        public string TypeName;
        public int AttackSpeed;
        public int damagePerSecond;
        public int minDamage;
        public int maxDamage;
        public int value;
        public int levelRequirment;
        public int LevelRequirmentTypeID;
        public IntVector2 Size;
        public Sprite Icon;
    }

    public List<ItemData> dbList = new List<ItemData>();

    /// <summary>
    /// Creates A Database of items into memmorey.
    /// </summary>
    /// <param name="file">The Json File to load.</param>
    private void LoadDb(TextAsset file)
    {

        var jsonString = file.text;
        ItemClass[] itemDB = JsonHelper.FromJson<ItemClass>(jsonString);
        foreach(ItemClass item in itemDB)
        {
            ItemData row = new ItemData();
            row.GlobalID = item.GlobalID;
            row.CategoryID = item.CategoryID;
            row.CategoryName = item.CategoryName;
            row.TypeID = item.TypeID;
            row.TypeName = item.TypeName;

            TypeNameList.Add(row.TypeName);
            row.Size = new IntVector2(item.SizeX, item.SizeY);
            row.Icon = Resources.Load<Sprite>("ItemIcons/" + item.TypeName);
      //      print("Item Icon Resource: " + row.Icon);

            row.AttackSpeed = item.AttackSpeed;
            row.damagePerSecond = item.DPS;

            row.minDamage = item.MinDamage;
            row.maxDamage = item.MaxDamage;

            row.levelRequirment = item.levelRequirment;
            row.LevelRequirmentTypeID = item.LevelRequirmentTypeID;
            row.value = item.Value;

      //      print("Item Value: " + item.Value);

            //  Debug.Log("Resource?: " + row.Icon);

            dbList.Add(row);
        }
    }


    public void PassItemData(ref ItemClass item)
    {
        int ID = item.GlobalID;
        item.CategoryID = dbList[ID].CategoryID;
        item.equipmentSlot = (ItemType)dbList[ID].CategoryID;
        item.CategoryName = dbList[ID].CategoryName;
        item.TypeID = dbList[ID].TypeID;
        item.TypeName = dbList[ID].TypeName;


        item.AttackSpeed = dbList[ID].AttackSpeed;
        item.DPS = dbList[ID].damagePerSecond;
        item.MinDamage = dbList[ID].minDamage;
        item.MaxDamage = dbList[ID].maxDamage;
        item.levelRequirment = dbList[ID].levelRequirment;
        item.LevelRequirmentTypeID = dbList[ID].LevelRequirmentTypeID;
        item.Value = dbList[ID].value;

        item.Size = dbList[ID].Size;
        item.Icon = dbList[ID].Icon;
    }

}

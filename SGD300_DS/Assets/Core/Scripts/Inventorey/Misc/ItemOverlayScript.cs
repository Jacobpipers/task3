﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
/// <summary>
/// TODO
/// </summary>
[RequireComponent(typeof(CanvasGroup))]
public class ItemOverlayScript : MonoBehaviour {

    public TextMeshProUGUI nameText, descriptionText, weaponTypeText, attributeText, requiredText, bonusText, valueText;
    public Sprite nullSprite;
    private float slotSize;


    /// <summary>
    /// 
    /// </summary>
    private void Start()
    {
    //    ItemButtonScript.overlayScript = this;
        SlotSectorScript.overlayScript = this;
        UpdateOverlay(null);
        slotSize = 50; //GameObject.FindGameObjectWithTag("InvenPanel").GetComponent<InvenGridScript>().slotSize;
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    public void UpdateOverlay(ItemClass item)
    {
        var requiredString = "";
        if (item != null)
        {
            // Name Of Item
            nameText.text = item.TypeName;

            descriptionText.text = "" + item.AttackSpeed;
            weaponTypeText.text = "" + item.CategoryName;



            switch ((ItemType)item.CategoryID)
            {

                case ItemType.Weapon: // Weapon
                case ItemType.RangedWeapon: // Range Weapon
                    attributeText.text = "Damage: " + item.MinDamage + " To " + item.MaxDamage;
                    break;

                case ItemType.BodyArmour:
                case ItemType.Boots:
                case ItemType.Gloves:
                case ItemType.Helmet:
                case ItemType.Sheild:
                    attributeText.text = "Armour: " + item.DPS;
                    break;

                case ItemType.Rings:
                    attributeText.text = "This ring does nothing";
                    break;

                case ItemType.Amulet:
                    attributeText.text = "This is a amulet";
                    break;

                case ItemType.Other:
                    switch (item.TypeID)
                    {
                        case 0:
                            attributeText.text = "Heals: " + item.DPS + " Health";
                            break;
                        case 1:
                            attributeText.text = "Heals: " + item.DPS + " Mana";
                            break;
                        case 2:
                            attributeText.text = "";
                            attributeText.gameObject.SetActive(false);
                            descriptionText.text = "What a pretty gem!";
                            break;
                    }

                    break;
            }





            requiredString = "Required: ";
            switch (item.LevelRequirmentTypeID)
            {
                case -1: requiredString = ""; break;
                case 0: requiredString += "Strength"; break;
                case 1: requiredString += "Dexterity"; break;
                case 2: requiredString += "Intelligence"; break;
                case 3: requiredString += "Melee"; break;
                case 4: requiredString += "Ranged"; break;
                case 5: requiredString += "Nature Magic"; break;
                case 6: requiredString += "Combat Magic"; break;
                case 7: requiredString += "Armour Rating"; break;
            }

            if (requiredString != "")
            {
                requiredText.gameObject.SetActive(true);
                requiredText.text = requiredString + " " + item.levelRequirment;
            }
            else
            {
                requiredText.text = "";
                requiredText.gameObject.SetActive(false);
            }


            switch (item.qualityInt)
            {
                case 0: nameText.color = Color.gray; break;
                case 1: nameText.color = Color.white; break;
                case 2: nameText.color = new Color(0.5f, 0.5f, 1f, 1f); break;
                case 3: nameText.color = Color.yellow; break;
                default: break;
            }

            valueText.text = "Value: " + item.Value;

            // Set Active when use.
            bonusText.gameObject.SetActive(false);

        }
        else
        {
            nameText.text = null;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="item"></param>
    /// <param name="ID"></param>
    /// <param name="lvl"></param>
    /// <param name="quality"></param>
    public void UpdateOverlay2(ItemClass item, bool ID, bool lvl, bool quality)
    {
        if (item != null)
        {
            // Name Of Item
            nameText.text = item.TypeName;

            descriptionText.text = "" + item.AttackSpeed;
            weaponTypeText.text = "" + item.CategoryName;



            switch ((ItemType)item.CategoryID)
            {

                case ItemType.Weapon: // Weapon
                case ItemType.RangedWeapon: // Range Weapon
                    attributeText.text = "Damage: " + item.MinDamage + " To " + item.MaxDamage;
                    break;

                case ItemType.BodyArmour:
                case ItemType.Boots:
                case ItemType.Gloves:
                case ItemType.Helmet:
                case ItemType.Sheild:
                    attributeText.text = "Armour: " + item.DPS;
                    break;

                case ItemType.Rings:
                    attributeText.text = "This ring does nothing";
                    break;

                case ItemType.Amulet:
                    attributeText.text = "This is a amulet";
                    break;

                case ItemType.Other:
                    switch (item.TypeID)
                    {
                        case 0:
                            attributeText.text = "Heals: " + item.DPS + " Health";
                            break;
                        case 1:
                            attributeText.text = "Heals: " + item.DPS + " Mana";
                            break;
                        case 2:
                            attributeText.text = "";
                            break;
                    }

                    break;
            }





            var requiredString = "Required: ";
            switch (item.LevelRequirmentTypeID)
            {
                case -1: requiredString = ""; break;
                case 0: requiredString += "Strength"; break;
                case 1: requiredString += "Dexterity"; break;
                case 2: requiredString += "Intelligence"; break;
                case 3: requiredString += "Melee"; break;
                case 4: requiredString += "Ranged"; break;
                case 5: requiredString += "Nature Magic"; break;
                case 6: requiredString += "Combat Magic"; break;
                case 7: requiredString += "Armour Rating"; break;
            }

            if (requiredString != "")
                requiredText.text = requiredString + " " + item.levelRequirment;


            switch (item.qualityInt)
            {
                case 0: nameText.color = Color.gray; break;
                case 1: nameText.color = Color.white; break;
                case 2: nameText.color = new Color(0.5f, 0.5f, 1f, 1f); break;
                case 3: nameText.color = Color.yellow; break;
                default: break;
            }

            valueText.text = "" + item.Value;

        }
        else
        {
            nameText.text = null;
        }
    }
}

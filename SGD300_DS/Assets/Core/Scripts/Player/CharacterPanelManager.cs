﻿/// <summary>
/// 
/// Created by Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TMPro;
using UnityEngine;

public class CharacterPanelManager : MonoBehaviour
{
    [HideInInspector] public PlayerInfo playerInfo;
    
    // Refrences
    public GameObject statScreenRef;
    public GameObject equipmentRef;

    // Base Stats Text
    public TextMeshProUGUI characterNameText;
    public TextMeshProUGUI StrengthText;
    public TextMeshProUGUI dexterityText;
    public TextMeshProUGUI intelligenceText;


    // Skills Text
    public TextMeshProUGUI meleeText;
    public TextMeshProUGUI rangedText;
    public TextMeshProUGUI natureMagicText;
    public TextMeshProUGUI combatMagicText;
    public TextMeshProUGUI melleeDamageText;
    public TextMeshProUGUI rangedDamageText;
    public TextMeshProUGUI armorRatingText;

    // Gold Text
    public TextMeshProUGUI goldText;

    /// <summary>
    /// Updates the UI To Show the Current values of the player.
    /// </summary>
    public void UpdateUI()
    {
        // Base Stats
        if (playerInfo == null)
            return;
        characterNameText.SetText(playerInfo.name);
        StrengthText.SetText(playerInfo.totalStrength.ToString());
        dexterityText.SetText(playerInfo.stats.dexterity.ToString());
        intelligenceText.SetText(playerInfo.stats.intelligence.ToString());

        // Skills
        meleeText.SetText(playerInfo.meleeSkill.ToString());
        rangedText.SetText(playerInfo.rangeSkill.ToString());
        natureMagicText.SetText(playerInfo.natureMagicskill.ToString());
        combatMagicText.SetText(playerInfo.combatMagicSkill.ToString());

        // Damage Ranges
        melleeDamageText.SetText("" + playerInfo.meleeDamage.x.ToString() + "-" + playerInfo.meleeDamage.y.ToString());
        rangedDamageText.SetText("" + playerInfo.rangeDamage.x.ToString() + "-" + playerInfo.rangeDamage.y.ToString());
        armorRatingText.SetText("" + playerInfo.totalDefence.ToString());
    }

    /// <summary>
    /// 
    /// </summary>
    void Update()
    {
        UpdateUI();
    }


}

﻿/// <summary>
/// Not In Use.
/// Created by Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof(CanvasGroup))]
public class OpenMenuItem : MonoBehaviour
{

    public virtual void OpenWindow()
    {
        var canvasGroup = transform.GetComponent<CanvasGroup>();
        canvasGroup.interactable = true;
        canvasGroup.blocksRaycasts = true;
        canvasGroup.alpha = 1f;
    }

    public virtual void CloseWindow()
    {
        var canvasGroup = transform.GetComponent<CanvasGroup>();
        canvasGroup.interactable = false;
        canvasGroup.blocksRaycasts = false;
        canvasGroup.alpha = 0f;

    }



}

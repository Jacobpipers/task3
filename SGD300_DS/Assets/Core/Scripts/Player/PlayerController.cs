﻿/// <summary>
/// This Handles the Players Logic 
/// This includes the Party Managment.
/// Created Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// The MiscParent Gets Assigned to the Object called miscParent on the Canvas.
    /// </summary>
    public GameObject miscParent;
    public GameObject characterPanelPrefab;
    public Transform uiParent;
    /// <summary>
    /// The Player Prefab.
    /// </summary>
    public GameObject player;
    /// <summary>
    /// A list of the current party members.
    /// </summary>
    [SerializeField] private List<GameObject> partyMembers = new List<GameObject>();

    /// <summary>
    /// Used To Get the current partymembers
    /// </summary>
    public List<GameObject> PartyMembers
    {
        get { return partyMembers; }
    }

    private List<GameObject> partyMemberProfiles = new List<GameObject>();
    private int index = 0;

    /// <summary>
    /// 
    /// </summary>
    void Start()
    {
        UpdatePartyMembersUI();
    }

    /// <summary>
    /// Removes all the party members UI.
    /// </summary>
    public void RemoveAllMembers()
    {
        index = 0;
        foreach (var memberProfile in partyMemberProfiles)
        {
            Destroy(memberProfile);
        }
    }

    /// <summary>
    /// Adds all the Initial Party Members UI.
    /// </summary>
    public void UpdatePartyMembersUI()
    {
        foreach (GameObject member in partyMembers)
        {
        //    print("Should print " + PartyMembers.Count.ToString() + " : " + member);
            AddMemeberUI(member);
        }
    }



    /// <summary>
    /// Test Adding a member to the party and removing member.
    /// </summary>
    void Update()
    {
        // Test Code;
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            if (partyMembers.Count < 6)
            {
                var newPlayer = Instantiate(player);
                AddPartyMember(newPlayer);
            }
        }
        else if(Input.GetKeyDown(KeyCode.Alpha2)) {
           bool doesRemove = RemovePartyMember(partyMembers[partyMembers.Count - 1]);
            if (doesRemove)
            {
                RemoveMembersUI(partyMemberProfiles[partyMemberProfiles.Count - 1]);
            }
        }


    }

    /// <summary>
    /// Adds a party members UI and assign the values to the objects.
    /// </summary>
    /// <param name="member">The Party Member to be The prefab of player has the scripts that need to be on it to work.</param>
    void AddMemeberUI(GameObject member)
    {
        // Creates the Character UI Panel
        GameObject characterPanel = Instantiate(characterPanelPrefab, new Vector3(30, 1050 - (150 * index)), Quaternion.identity);
        characterPanel.transform.SetParent(uiParent, true);
        partyMemberProfiles.Add(characterPanel);

        // Create a refrence to the Inventorey Grid Manager and Assign the correct objects/scripts
        InvenGridManager invenGridManager = characterPanel.GetComponentInChildren<InvenGridManager>();
        invenGridManager.playerController = gameObject;
        invenGridManager.itemEquipPool = GameObject.Find("ItemEquip").GetComponent<ObjectPoolScript>();
        invenGridManager.overlayScript = GameObject.Find("ItemToolTip").GetComponent<ItemOverlayScript>();
        // Set the position of the Inventorey and set it.
        invenGridManager.GetComponent<RectTransform>().position = new Vector3(300, 325); // Set Y To Same Level

      
        // Get A refrence to the Character that this info relatess to.
        CharactersInventoreyInfo charactersInventoreyInfo = member.GetComponent<CharactersInventoreyInfo>();
        // Assign inventorey to the character.
        charactersInventoreyInfo.invRef = invenGridManager;
        charactersInventoreyInfo.invRef.InventoreyOwner = member;
        // Check the items the player has stored and add it to ui.
        var items = charactersInventoreyInfo.items;
        foreach (var item in items)
        {
            invenGridManager.AddItemToInv(item);
        }
        // Clear Items. TODO: Need to add add on INventorey and game destruction 
        //items.Clear();


        //  Gets a refrence to the character panel 
        var characterPanelMan = characterPanel.GetComponent<CharacterPanelManager>();

        // Set
        var statPos = characterPanelMan.statScreenRef.GetComponent<RectTransform>().position = new Vector3(550, 1030);


        // Gets a refrence to the global stats: Gold and any other info specific to the player and not the character.
        var gloabalStats = GetComponent<GlobalStats>();
        // Updates the gold display in character info panels 
        gloabalStats.goldText.Add(characterPanelMan.goldText);
        gloabalStats.UpdateGoldUI();

        // Get the Current Characters Info such as strength ... and assign to the player info on the character panel.
        var partyMemberInfo = member.GetComponent<PlayerInfo>();
        characterPanelMan.playerInfo = partyMemberInfo;
        // Get Refrence to the Equipment slots on the character UI.
        var EquipmentSlots = characterPanelMan.equipmentRef.GetComponentsInChildren<SlotScript>();

      
        // Loop Through each equipment slot and add it to the playerInfo 
        foreach (var slot in EquipmentSlots)
        {
            partyMemberInfo.equipmentSlots.Add(slot);
        }
        // Load the current stats based on equipment and update the UI to reflect that.
        partyMemberInfo.LoadStats();

        // Get A refrence to All the Health bars in the characters UI and update them.
        HealthBar[] healthBars = characterPanel.GetComponentsInChildren<HealthBar>();
        foreach (HealthBar healthBar in healthBars)
        {
            healthBar.SetTarget(member);
        }
        // Get A refrence to All the Mana bars in the characters UI and update them.
        ManaBar[] manaBars = characterPanel.GetComponentsInChildren<ManaBar>();
        foreach (ManaBar manaBar in manaBars)
        {
            manaBar.SetTarget(member);
        }

        index++;
    }


    /// <summary>
    /// Call to add a party member to the group.
    /// </summary>
    /// <param name="member">The Party Member to be The prefab of player has the scripts that need to be on it to work.</param>
    /// <returns></returns>
    public bool AddPartyMember(GameObject member)
    {
        if(partyMembers.Count < 6)
        {
            partyMembers.Add(member);
            // RemoveAllMembers();
            AddMemeberUI(member);
           // UpdatePartyMembersUI();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Removes the party member 
    /// </summary>
    /// <param name="member">Pass a party member that you want to remove.</param>
    /// <returns></returns>
    public bool RemovePartyMember(GameObject member)
    {
        if (partyMembers.Count == 1)
            return false;

        partyMembers.Remove(member);
      
        return true;
    }

    /// <summary>
    /// Remove A single Members UI.
    /// </summary>
    /// <param name="memberProfile"></param>
    public void RemoveMembersUI(GameObject memberProfile)
    {
        index--;
        partyMemberProfiles.Remove(memberProfile);
        Destroy(memberProfile);
    }

}

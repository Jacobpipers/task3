﻿/// <summary>
/// This Script handles the stats and skill data and logic 
/// 
///  Written By Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo : MonoBehaviour
{

    [SerializeField] public BaseStats stats;
    [SerializeField] public int curGold;
    [SerializeField] public IntVector2 meleeDamage;
    [SerializeField] public IntVector2 rangeDamage;
    [SerializeField] public int totalDefence;
    [SerializeField] public List<SlotScript> equipmentSlots;

    // Skills
    [SerializeField] public int meleeSkill;
    [SerializeField] public int rangeSkill;
    [SerializeField] public int natureMagicskill;
    [SerializeField] public int combatMagicSkill;


    [HideInInspector] public int totalStrength;

    private IntVector2 meleeSkillEXP = new IntVector2(0, 15);
    private IntVector2 rangeSkillEXP = new IntVector2(0, 15);
    private IntVector2 natureMagicSkillEXP = new IntVector2(0, 15);
    private IntVector2 combatMagicSkillEXP = new IntVector2(0, 15);

    private List<ItemClass> defenceItems;


    /// <summary>
    /// 
    /// </summary>
    /// <param name="_stats"></param>
    public void InitStats(BaseStats _stats)
    {
        stats = _stats;
        meleeSkill = 1;
        rangeSkill = 1;
        natureMagicskill = 1;
        combatMagicSkill = 1;
        meleeSkillEXP = new IntVector2(0, 15);
        totalStrength = CalculateTotalStrength();
    }


    private void Update()
    {
       if(GetCurrentHealth > GetMaxHealth)
        {
            stats.health = (int)GetMaxHealth;
        }
    }

    /// <summary>
    /// Goes through each Equipment slot and checks the type and calculates the modified stats.
    /// </summary>
    public void LoadStats()
    {
        defenceItems = new List<ItemClass>();
   //     print("LoadStats Start");
        totalStrength = CalculateTotalStrength();
        foreach (var slot in equipmentSlots)
        {
            switch (slot.slotInfo.allowedItems)
            {
                case ItemType.Weapon:
                   
                    meleeDamage = CalculateMeleeDamage(slot.slotInfo.storedItemClass);
                    break;

                case ItemType.RangedWeapon:
                    rangeDamage = CalculateRangeDamage(slot.slotInfo.storedItemClass);
                    break;

                case ItemType.Sheild:
                case ItemType.BodyArmour:
                case ItemType.Helmet:
                case ItemType.Gloves:
                case ItemType.Boots:
                    //
                    if (slot.slotInfo.storedItemClass != null)
                        defenceItems.Add(slot.slotInfo.storedItemClass);
                    break;
            }

        }
        totalDefence = CalculateArmourRating(defenceItems);
 //       print("LoadStats End");
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="armour"></param>
    /// <returns></returns>
    public int CalculateArmourRating(List<ItemClass> armours)
    {
        if (armours == null)
            return stats.dexterity;
        var weaponDefence = 0;
        foreach(var armour in armours)
        {
        //    print(armour.TypeName.ToString());
            weaponDefence += armour.DPS; /// TODO: Chenge this to correect Value. like Defence.
        }
        return stats.dexterity + weaponDefence;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="weapon"></param>
    /// <returns></returns>
    public IntVector2 CalculateMeleeDamage(ItemClass weapon)
    {
        var min = totalStrength;
        var max = 1 + totalStrength;

        if (weapon != null && weapon.CategoryName != "")
        {
            print("Null should not exist");
            print(weapon.GlobalID);
            min = totalStrength + weapon.MinDamage;
            max = totalStrength + weapon.MaxDamage;
           
        }
        return new IntVector2(min, max);
    }
    
  

    /// <summary>
    /// 
    /// </summary>
    /// <param name="weapon"></param>
    /// <returns></returns>
    public IntVector2 CalculateRangeDamage(ItemClass weapon)
    {
        var min = 0;
        var max = 0;

        if (weapon != null && weapon.CategoryName != "")
        {
            min = weapon.MinDamage;
            max = weapon.MaxDamage;
        }
        return new IntVector2(min, max);
    }

    /// <summary>
    /// Calculates the total strength that the character has based on certein values.
    /// </summary>
    /// <returns></returns>
    public int CalculateTotalStrength()
    {
        return stats.strength + meleeSkill; // TODO: Add any special stats.
    }


    public void IncreaseMeleeSkill(int amount)
    {
        meleeSkillEXP.x += amount;
        print(meleeSkillEXP.x.ToString() + " : " + meleeSkillEXP.y.ToString());
        if (meleeSkillEXP.x >= meleeSkillEXP.y)
        {
            meleeSkillEXP.x -= meleeSkillEXP.y;
            meleeSkillEXP.y = (Mathf.Max(1, meleeSkill) * 50); // Basic Maths Could use a log or better curve (this is linear.)
            meleeSkill++;
            LoadStats();
        }
    }


    public void IncreaseRangeSkill(int amount)
    {
        rangeSkillEXP.x += amount;
        print(rangeSkillEXP.x.ToString() + " : " + rangeSkillEXP.y.ToString());
        if (rangeSkillEXP.x >= rangeSkillEXP.y)
        {
            rangeSkillEXP.x -= rangeSkillEXP.y;
            rangeSkillEXP.y = (Mathf.Max(1, rangeSkill) * 50); // Basic Maths Could use a log or better curve (this is linear.)
            rangeSkill++;
            LoadStats();
        }
    }

    public void IncreaseNatureMagicSkill(int amount)
    {
        natureMagicSkillEXP.x += amount;
        print(natureMagicSkillEXP.x.ToString() + " : " + natureMagicSkillEXP.y.ToString());
        if (natureMagicSkillEXP.x >= natureMagicSkillEXP.y)
        {
            natureMagicSkillEXP.x -= natureMagicSkillEXP.y;
            natureMagicSkillEXP.y = (Mathf.Max(1, natureMagicskill) * 50); // Basic Maths Could use a log or better curve (this is linear.)
            natureMagicskill++;
            LoadStats();
        }
    }

    public void IncreaseCombatMagicSkill(int amount)
    {
        combatMagicSkillEXP.x += amount;
        print(combatMagicSkillEXP.x.ToString() + " : " + combatMagicSkillEXP.y.ToString());
        if (combatMagicSkillEXP.x >= combatMagicSkillEXP.y)
        {
            combatMagicSkillEXP.x -= combatMagicSkillEXP.y;
            combatMagicSkillEXP.y = (Mathf.Max(1, combatMagicSkill) * 50); // Basic Maths Could use a log or better curve (this is linear.)
            combatMagicSkill++;
            LoadStats();
        }
    }

    public float GetHealthPercentage()
    {
        return GetCurrentHealth / GetMaxHealth;
    }

    public void HealSelf(float amount)
    {
        if (stats.health + amount > stats.mHealth)
        {
            stats.health = stats.mHealth;
        }
        else
        {
            stats.health += (int)amount;
        }
    }

    public float GetCurrentHealth
    {
        get { return stats.health; }
    }

    public float GetMaxHealth
    {
        get { return stats.mHealth; }
    }


}

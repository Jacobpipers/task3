﻿/// <summary>
/// This Script will be used to save and spawn the characters current Inventorey.
/// Created By Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharactersInventoreyInfo : MonoBehaviour
{
    [SerializeField]
    public SlotClass[,] playersInvGrid;
    [SerializeField]
    public List<ItemClass> items = new List<ItemClass>();
    public InvenGridManager invRef;
    public IntVector2 gridSize;


    void Start()
    {
        playersInvGrid = new SlotClass[gridSize.x, gridSize.y];
        CreateSlots();

    }



    public void LoadItems()
    {

    }

    /// <summary>
    /// Store the Current Incentorey Info onto the player data.
    /// </summary>
    /// <param name="slot"></param>
    public void AddItemToCharacterSlot(SlotClass slot)
    {
        playersInvGrid[slot.gridPos.x, slot.gridPos.y] = slot;
        print("Slot Add To PLayer" + playersInvGrid[slot.gridPos.x, slot.gridPos.y].storedItemClass.TypeName);
    }

    /// <summary>
    /// Creates the Data structure for the characters inventorey.
    /// </summary>
    private void CreateSlots()
    {
        for (int y = 0; y < gridSize.y; y++)
        {
            for (int x = 0; x < gridSize.x; x++)
            {
                playersInvGrid[x, y] = new SlotClass
                {
                    gridPos = new IntVector2(x, y),
                    allowedItems = ItemType.Other,
                    storedItemClass = new ItemClass(),
                    isOccupied = false,
                    storedItemStartPos = IntVector2.Zero,
                    storedItemSize = IntVector2.Zero,
                    storedItemObject = null
                };
            }
        }
    }



}

﻿/// <summary>
/// 
/// Written By Jacob Pipers
/// </summary>
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 
/// </summary>
[System.Serializable]
public struct BaseStats
{
    public int strength;
    public int dexterity;
    public int intelligence;
    public int armourRating;
    public int health;
    public int mHealth;
    public int mana;
    public int mMana;

   

    /// <summary>
    /// Used to Create a custom init at runtime such when a character is spawned.
    /// </summary>
    /// <param name="_strength"></param>
    /// <param name="_dexterity"></param>
    /// <param name="_intelligence"></param>
    /// <param name="_armourRating"></param>
    /// <param name="_health"></param>
    /// <param name="_mana"></param>
    public BaseStats(int _strength = 1, int _dexterity = 0, int _intelligence= 0 , int _armourRating = 0, int _health = 100, int _mana = 100)
    {
        strength = _strength;
        dexterity = _dexterity;
        intelligence = _intelligence;
        armourRating = _armourRating;
        health = _health;
        mana = _mana;
        mHealth = _health;
        mMana = _mana;
    }

}

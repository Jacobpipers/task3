﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script handles the mana of the object its attached to, aswell as feeding the increase and decrease of mana into the target.
/// Ryley Delaney - DelaneyRyley@gmail.com
/// Edited by Ruan Vermaak 22/08/19
/// </summary>
public class Mana : MonoBehaviour
{
    [SerializeField]
    private int maxMana = 100;
    [SerializeField]
    private int currentMana;

    /// <summary>
    /// The GetMaxMana property represents the scripholders maximum mana.
    /// </summary>
    /// <value>The GetMaxMana value returns the maximum Mana of the scriptholder</value>
    public int GetMaxMana
    {
        get { return maxMana; }
    }

    /// <summary>
    /// The GetCurrentMana property represents the scriptholders current Mana.
    /// </summary>
    /// <value>The GetCurrentMana property gets the current mana of the scriptholder.</value>
    public int GetCurrentMana
    {
        get { return currentMana; }
    }

    private void OnEnable()
    {
        currentMana = maxMana;
    }

    /// <summary>
    /// Initialises the mana values for the save system.
    /// </summary>
    /// <param name="currentMana"></param>
    /// <param name="maxMana"></param>
    public void LoadMana(int currentMana, int maxMana)
    {
        this.currentMana = currentMana;
        this.maxMana = maxMana;
    }

    void Update()
    {
        if (currentMana > maxMana)
        {
            currentMana = maxMana;
        }
    }

    /// <summary>
    /// A public method that decreases the scriptholders mana.
    /// </summary>
    /// <param name="manaUse"></param>
    public void ExpendMana(int manaUse)
    {
        currentMana -= manaUse;
    }



    /// <summary>
    /// A public method that increases the scriptholders mana but doesn't exceed their max Mana
    /// </summary>
    /// <param name="manaRestore"></param>
    public void RestoreMana(int manaRestore)
    {
        if (currentMana + manaRestore > maxMana)
        {
            currentMana = maxMana;
        }
        else
        {
            currentMana += manaRestore;
        }
    }

    /// <summary>
    /// A public method that returns a percentage of the scriptholders current mana over their maximum mana.
    /// </summary>
    /// <returns>The percentage between 0 and 1 of the player's current mana.</returns>
    public float GetManaPercentage()
    {

        return ((float)currentMana / (float)maxMana);
    }
}

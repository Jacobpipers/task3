﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// This script handles the manabar visual state and gathers data from the Target or player for the script. For mana Calculation see the "Mana" script
/// Ryley Delaney - DelaneyRyley@gmail.com
/// </summary>
public class ManaBar : MonoBehaviour
{
    [SerializeField]
    private Image manaImage;
    [SerializeField]
    private Text manaText;
    [SerializeField]
    private GameObject target;

    private Mana mana;

    void Start()
    {
        if (target == null)
            target = GameObject.FindGameObjectWithTag("Player");
       
    }

    void Update()
    {
        // if the target has a mana script, populate the mana bars information
        if (mana != null)
        {
            manaImage.fillAmount = mana.GetManaPercentage();
            manaText.text = mana.GetCurrentMana + " / " + mana.GetMaxMana;
        }
    }

    /// <summary>
    /// SetTarget will set the target member variable to the newTarget parameter.
    /// </summary>
    /// <param name="newTarget">A reference to a game object that should have a Mana component attached.</param>
    public void SetTarget(GameObject newTarget)
    {
        target = newTarget;
        mana = target.GetComponent<Mana>();
    }
}

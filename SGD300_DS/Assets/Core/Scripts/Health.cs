﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This script handles the health of the object its attached to, aswell as feeding the damage and healing data into the target.
/// Ryley Delaney - DelaneyRyley@gmail.com
/// Edited by Ruan Vermaak 22/08/19
/// </summary>
public class Health : MonoBehaviour
{
    [SerializeField]
    private int maxHealth = 100;
    [SerializeField]
    private int currentHealth;

    // By Kim
    public GameObject damageFloatingText;
    public float test_damage = 16;

    /// <summary>
    /// The GetMaxHealth property represents the scripholders maximum health.
    /// </summary>
    /// <value>The GetMaxHealth value returns the maximum health of the scriptholder</value>
    public int GetMaxHealth
    {
        get { return maxHealth; }
    }

    /// <summary>
    /// The GetCurrentHealth property represents the scriptholders current health.
    /// </summary>
    /// <value>The GetCurrentHealth property gets the current health of the scriptholder.</value>
    public int GetCurrentHealth
    {
        get { return currentHealth; }
    }

    private void OnEnable()
    {
        currentHealth = maxHealth;
    }

    /// <summary>
    /// Initialises the health values for the save system.
    /// </summary>
    /// <param name="currentHealth"></param>
    /// <param name="maxHealth"></param>
    public void LoadHealth(int currentHealth, int maxHealth)
    {
        this.currentHealth = currentHealth;
        this.maxHealth = maxHealth;
    }

    void Update()
    {
        // Ensure the players current Health cannot exceed their max Health
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }
        if(Input.GetKey(KeyCode.Q))
        {
            showFloatingText();
        }
    }

    /// <summary>
    /// A public method that decreases the scriptholders health.
    /// </summary>
    /// <param name="damage"></param>
    public void DamageSelf(int damage)
    {
        currentHealth -= damage;

        //By Kim
        // Add function to destroy character when hp is lower or equal to 0
        if (currentHealth <= 0)
        {
            Destroy(gameObject);
        }


        //if (damageFloatingText && currentHealth <= 0)
        //{
        //    Debug.Log("Damage Floating Text");
        //    showFloatingText();
        //}

    }

    // By Kim
    // Spawning floating Message
    public void showFloatingText()
    {
        var healthText = Instantiate(damageFloatingText, transform.position, Quaternion.identity, transform);
 
        healthText.GetComponent<TextMesh>().text = GetComponent<Health>().test_damage.ToString();
    }

    /// <summary>
    /// A public method that increases the scriptholders health but doesn't exceed their max Health
    /// </summary>
    /// <param name="healing"></param>
    public void HealSelf(int healing)
    {
        if (currentHealth + healing > maxHealth) 
        {
            currentHealth = maxHealth;
        }
        else
        {
            currentHealth += healing;
        }
    }

    /// <summary>
    /// A public method that returns a percentage of the scriptholders current health over their maximum health.
    /// </summary>
    /// <returns>The percentage between 0 and 1 of the player's current health.</returns>
    public float GetHealthPercentage()
    {
        return ((float)currentHealth / (float)maxHealth);
    }
}

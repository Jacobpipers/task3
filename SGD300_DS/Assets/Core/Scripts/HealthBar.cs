﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// This script handles the healthbar visual state and gathers data from the Target or player for the script. For Health Calculation see the "Health" script
/// Ryley Delaney - DelaneyRyley@gmail.com
/// </summary>
public class HealthBar : MonoBehaviour
{
    [SerializeField]
    private Image healthImage;
    [SerializeField]
    private Text healthText;
    [SerializeField]
    private GameObject target;

    private PlayerInfo health;

    void Start()
    {
        if (target == null)
            target = GameObject.FindGameObjectWithTag("Player");
    }

    void Update()
    {
        if(health != null)
        {
            healthImage.fillAmount = (health.GetHealthPercentage());
            healthText.text = health.GetCurrentHealth + " / " + health.GetMaxHealth;
        }
    //    Debug.LogWarning("Health update??? Target: " + health.health);
    }

    /// <summary>
    /// SetTarget will set the target member variable to the newTarget parameter.
    /// </summary>
    /// <param name="newTarget">A reference to a game object that should have a Health component attached.</param>
    public void SetTarget(GameObject newTarget)
    {
        target = newTarget;
        health = target.GetComponent<PlayerInfo>();
    }

}

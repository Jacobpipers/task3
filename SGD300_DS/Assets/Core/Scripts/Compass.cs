﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotates the UI element (Compass) on the Z axis according to
/// the Y axis of the referenced game object (Camera).
/// Attach this script component to the desired UI element (Compass) and
/// assign a game object (Camera) to get the rotational values from.
/// Made by Ruan Vermaak.
/// </summary>
public class Compass : MonoBehaviour
{
    [SerializeField] Transform cameraTransform;
    private Vector3 compassVector;

    // Update is called once per frame
    void Update()
    {
        // Get the rotation value of the referenced camera
        compassVector.z = cameraTransform.eulerAngles.y;
        // Set the rotation value of the compass image
        transform.localEulerAngles = compassVector;
    }
}

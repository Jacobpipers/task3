﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerHealth : MonoBehaviour
{
    [SerializeField] float hitPoints = 10f;


    public void TakeDamage(float damage) // allows player to receive damage
    {
        hitPoints -= damage;
        if (hitPoints <= 0)
        {
            Debug.Log("you have died a dishonorable death");
            //Destroy(gameObject); // destroys the payer when health reaches zero
        }
    }
}

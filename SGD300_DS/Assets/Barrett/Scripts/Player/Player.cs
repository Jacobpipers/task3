﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// this script should be ignored and is for Barrett's testing purposes only
/// </summary>
public class Player : MonoBehaviour
{
    [SerializeField] float jumpHeight = 50f;
    [SerializeField] float movementSpeed = 10f;
    [SerializeField] float rotationSpeed = 1.0f;

    private float rotX;
    private float rotY;
    private float rotZ;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.LeftShift) && Input.GetKey("w")) //  if left shift and w are pressed together sprint
        {
            transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * movementSpeed * 2.5f;
        }
        else if (Input.GetKey("w") && !Input.GetKey(KeyCode.LeftShift)) // else just move forwards
        {
            transform.position += transform.TransformDirection(Vector3.forward) * Time.deltaTime * movementSpeed;
        }
        else if (Input.GetKey(KeyCode.S)) // move backwards
        {
            transform.position -= transform.TransformDirection(Vector3.forward) * Time.deltaTime * movementSpeed;
        }

        if (Input.GetKey("a") && !Input.GetKey ("d")) // move left
        {
            transform.position += transform.TransformDirection(Vector3.left) * Time.deltaTime * movementSpeed;
        }
        else if (Input.GetKey("d") && !Input.GetKey("a")) // move right
        {
            transform.position += transform.TransformDirection(Vector3.right) * Time.deltaTime * movementSpeed;
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space)) // jump
        {
            if (transform.position.y <= 1.05f)
            {            
                GetComponent<Rigidbody>().AddForce(Vector3.up * jumpHeight);
            }
        }

        rotX -= Input.GetAxis("Mouse Y") * Time.deltaTime * rotationSpeed; // rotation controls
        rotY += Input.GetAxis("Mouse X") * Time.deltaTime * rotationSpeed;

        if(rotX <= -90) { // stops player from rotating a full circle up or down
            rotX = -90;
        }
        else if (rotX > 90) {
            rotX = 90;
        }

        transform.rotation = Quaternion.Euler(0, rotY, 0);
        GameObject.FindGameObjectWithTag("MainCamera").transform.rotation = Quaternion.Euler(rotX, rotY, 0);
    }
}

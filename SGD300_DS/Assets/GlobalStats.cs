﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class GlobalStats : MonoBehaviour
{

    public int Gold;

    [HideInInspector] public List<TextMeshProUGUI> goldText;


    public void IncreaseGold(int amount)
    {
        Gold += amount;
    }

    public void UpdateGoldUI()
    {
        if(goldText != null)
        {
            foreach(var text in goldText)
            {
                text.SetText("" + Gold);
            }
        }
    }


    

}
